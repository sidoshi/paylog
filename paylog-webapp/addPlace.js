const fs = require('fs');

const placesFilePath = require.resolve('./src/utils/places.json');
const places = new Set(require('./src/utils/places.json'));

const placesToAdd = process.argv.splice(2);
if (!placesToAdd) {
  console.log('Please provide places to add');
  process.exit(1);
}

placesToAdd.forEach(place => {
  places.add(place.split('_').join(' '));
});

fs.writeFileSync(
  placesFilePath,
  JSON.stringify(Array.from(places).sort(), null, 2)
);

console.log(`${placesToAdd} added sucessfully`);
