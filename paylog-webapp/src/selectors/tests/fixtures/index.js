export const state = {
  labels: {
    allLabels: {
      '1': {
        senderPlace: 'mahuva',
        receiverPlace: 'rajula',
        sender: {
          name: 'foo',
        },
        receiver: {
          name: 'bar',
        },
        amount: 20000,
        charge: 100,
        ownProfit: 35,
        headOfficeProfit: 65,
      },
      '2': {
        senderPlace: 'mahuva',
        receiverPlace: 'mumbai',
        sender: {
          name: 'foo',
        },
        receiver: {
          name: 'bar',
        },
        amount: 10000,
        charge: 50,
        ownProfit: 17.5,
        headOfficeProfit: 32.5,
      },
      '3': {
        senderPlace: 'mahuva',
        receiverPlace: 'talaja',
        sender: {
          name: 'foo',
        },
        receiver: {
          name: 'bar',
        },
        amount: 10000,
        charge: 50,
        ownProfit: 17.5,
        headOfficeProfit: 32.5,
      },
      '4': {
        senderPlace: 'talaja',
        receiverPlace: 'mahuva',
        sender: {
          name: 'foo',
        },
        receiver: {
          name: 'bar',
        },
        amount: 20000,
        charge: 100,
        ownProfit: 35,
        headOfficeProfit: 65,
      },
      '5': {
        senderPlace: 'rajula',
        receiverPlace: 'mahuva',
        sender: {
          name: 'foo',
        },
        receiver: {
          name: 'bar',
        },
        amount: 2000,
        charge: 100,
        ownProfit: 35,
        headOfficeProfit: 65,
      },
      '6': {
        senderPlace: 'mumbai',
        receiverPlace: 'mahuva',
        sender: {
          name: 'foo',
        },
        receiver: {
          name: 'bar',
        },
        amount: 200,
        charge: 100,
        ownProfit: 35,
        headOfficeProfit: 65,
      },
    },
    allLabelIds: ['1', '2', '3', '4', '5', '6'],
    searchTerm: '',
    sortTerm: 'DATE_HIGH_TO_LOW',
  },
  self: {
    place: 'mahuva',
    closingBalance: 1000,
    officeDate: 100000,
  },
  addLabelForm: {
    senderPlace: '',
    receiverPlace: '',
    senderDisabled: true,
    receiverDisabled: false,
    amount: 0,
    charge: 0,
    senderName: '',
    senderNumber: '',
    receiverName: '',
    receiverNumber: '',
  },
}
