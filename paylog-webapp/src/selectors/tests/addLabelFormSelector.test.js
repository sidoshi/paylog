import { getAddLabelFormState } from '../'
import { state } from './fixtures'

describe('getAddLabelFormState', () => {
  it('should return all labels from a given state', () => {
    expect(getAddLabelFormState(state)).toEqual(state.addLabelForm)
  })
})
