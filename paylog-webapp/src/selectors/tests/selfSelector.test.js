import { getSelfPlace, getClosingBalance, getOfficeDate } from '../'
import { state } from './fixtures'

describe('getSelfLabels', () => {
  it('should return self place from a given state', () => {
    expect(getSelfPlace(state)).toEqual(state.self.place)
  })
})
describe('getClosingBalance', () => {
  it('should return closing balance from a given state', () => {
    expect(getClosingBalance(state)).toEqual(state.self.closingBalance)
  })
})
describe('getOfficeDate', () => {
  it('should return office date from a given state', () => {
    expect(getOfficeDate(state)).toEqual(state.self.officeDate)
  })
})
