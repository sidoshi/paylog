import immutable from 'object-path-immutable'

import {
  getAllLabels,
  getAllLabelIds,
  getReceivedLabelIds,
  getSentLabelIds,
  getSearchTerm,
  getSortTerm,
  getSortedLabelIds,
  getVisibleLabelIds,
} from '../'
import { state } from './fixtures'

describe('getAllLabels', () => {
  it('should return all labels from a given state', () => {
    expect(getAllLabels(state)).toEqual(state.labels.allLabels)
  })
})

describe('getAllLabelIds', () => {
  it('should return all labels from a given state', () => {
    expect(getAllLabelIds(state)).toEqual(
      state.labels.allLabelIds.slice(0).reverse(),
    )
  })
})

describe('getReceivedLabels', () => {
  it('should return all received labels from a given state', () => {
    expect(getReceivedLabelIds(state)).toEqual(['6', '5', '4'])
  })
})

describe('getSentLabels', () => {
  it('should return all sent labels from a given state', () => {
    expect(getSentLabelIds(state)).toEqual(['3', '2', '1'])
  })
})

describe('getSearchTerm', () => {
  it('should return searchTerm from a given state', () => {
    expect(getSearchTerm(state)).toEqual('')
  })
})

describe('getSortTerm', () => {
  it('should return sortTerm from a given state', () => {
    expect(getSortTerm(state)).toEqual('DATE_HIGH_TO_LOW')
  })
})

describe('getSortedLableIds', () => {
  it('should return allLabelIds as default case', () => {
    let _state = immutable.set(state, 'labels.sortTerm', '')
    expect(getSortedLabelIds(_state)).toEqual(['6', '5', '4', '3', '2', '1'])
  })
  it('should sort Date high-to-low properly', () => {
    let _state = immutable.set(state, 'labels.sortTerm', 'DATE_HIGH_TO_LOW')
    expect(getSortedLabelIds(_state)).toEqual(['6', '5', '4', '3', '2', '1'])
  })
  it('should sort Date low-to-high properly', () => {
    let _state = immutable.set(state, 'labels.sortTerm', 'DATE_LOW_TO_HIGH')
    expect(getSortedLabelIds(_state)).toEqual(['1', '2', '3', '4', '5', '6'])
  })
  it('should sort Amount high-to-low properly', () => {
    let _state = immutable.set(state, 'labels.sortTerm', 'AMOUNT_HIGH_TO_LOW')
    expect(getSortedLabelIds(_state)).toEqual(['4', '1', '3', '2', '5', '6'])
  })
  it('should sort Amount low-to-high properly', () => {
    let _state = immutable.set(state, 'labels.sortTerm', 'AMOUNT_LOW_TO_HIGH')
    expect(getSortedLabelIds(_state)).toEqual(['6', '5', '3', '2', '4', '1'])
  })
})

describe('getVisibleLableIds', () => {
  it('should return allLabelIds as default case', () => {
    let _state = immutable.set(state, 'labels.searchTerm', '')
    expect(getVisibleLabelIds(_state)).toEqual(['6', '5', '4', '3', '2', '1'])
  })
  it('should search on senderPlace and receiverPlace properly', () => {
    let _state
    _state = immutable.set(state, 'labels.searchTerm', 'rajula')
    expect(getVisibleLabelIds(_state)).toEqual(['5', '1'])
    _state = immutable.set(state, 'labels.searchTerm', 'mumbai')
    expect(getVisibleLabelIds(_state)).toEqual(['6', '2'])
  })
  it('should search on amount properly', () => {
    let _state
    _state = immutable.set(state, 'labels.searchTerm', '2000')
    expect(getVisibleLabelIds(_state)).toEqual(['5'])
    _state = immutable.set(state, 'labels.searchTerm', '2')
    expect(getVisibleLabelIds(_state)).toEqual([])
    _state = immutable.set(state, 'labels.searchTerm', '200')
    expect(getVisibleLabelIds(_state)).toEqual(['6'])
    _state = immutable.set(state, 'labels.searchTerm', '20000')
    expect(getVisibleLabelIds(_state)).toEqual(['4', '1'])
  })
  it('should do combination searches properly', () => {
    let _state
    _state = immutable.set(state, 'labels.searchTerm', '20000,talaja')
    expect(getVisibleLabelIds(_state)).toEqual(['4'])
    _state = immutable.set(state, 'labels.searchTerm', '10000')
    expect(getVisibleLabelIds(_state)).toEqual(['3', '2'])
    _state = immutable.set(state, 'labels.searchTerm', 'mumbai,10000')
    expect(getVisibleLabelIds(_state)).toEqual(['2'])
  })
})
