import {
  getCreditEntries,
  getDebitEntries,
  getCreditTotal,
  getDebitTotal,
} from '../'
import { state } from './fixtures'

describe('getCreditEntries', () => {
  it('should give all credit entries', () => {
    expect(getCreditEntries(state)).toEqual([
      { place: 'mumbai', amount: 235 },
      { place: 'rajula', amount: 2035 },
      { place: 'talaja', amount: 20035 },
    ])
  })
})

describe('getCreditTotal', () => {
  it('should give all credit entries sum', () => {
    expect(getCreditTotal(state)).toEqual(22305)
  })
})

describe('getDebitTotal', () => {
  it('should give all credit entries sum', () => {
    expect(getDebitTotal(state)).toEqual(41130)
  })
})

describe('getDebitEntries', () => {
  it('should give all debit entries', () => {
    expect(getDebitEntries(state)).toEqual([
      { place: 'closing balance', amount: 1000 },
      { place: 'talaja', amount: 10032.5 },
      { place: 'mumbai', amount: 10032.5 },
      { place: 'rajula', amount: 20065 },
    ])
  })
})
