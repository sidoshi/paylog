import { getExchange, getProfit, getBalance } from '../'
import { state } from './fixtures'

describe('getExchange', () => {
  it('should give total of all labels', () => {
    expect(getExchange(state)).toEqual(62200)
  })
})

describe('getProfit', () => {
  it('should give total of profit from all labels', () => {
    expect(getProfit(state)).toEqual(175)
  })
})

describe('getBalance', () => {
  it('should give balance of account', () => {
    expect(getBalance(state)).toEqual(18825)
  })
})
