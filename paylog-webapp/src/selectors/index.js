import { createSelector } from 'reselect'

import type { State } from 'reducers'

export const getAllLabels = (state: State) => state.labels.allLabels

export const getAllLabelIds = (state: State) =>
  [...state.labels.allLabelIds].reverse()

export const getSelfPlace = (state: State) => state.self.place

export const getClosingBalance = (state: State) => state.self.closingBalance

export const getOfficeDate = (state: State) => state.self.officeDate

export const getSearchTerm = (state: State) => state.labels.searchTerm

export const getSortTerm = (state: State) => state.labels.sortTerm

export const getSortedLabelIds = createSelector(
  [getSortTerm, getAllLabelIds, getAllLabels],
  (sortTerm, allLabelIds, allLabels) => {
    switch (sortTerm) {
      case 'DATE_HIGH_TO_LOW':
        return allLabelIds.slice(0)
      case 'DATE_LOW_TO_HIGH':
        return allLabelIds.slice(0).reverse()
      case 'AMOUNT_HIGH_TO_LOW':
        return allLabelIds
          .slice(0)
          .sort((x, y) => allLabels[y].amount - allLabels[x].amount)
      case 'AMOUNT_LOW_TO_HIGH':
        return allLabelIds
          .slice(0)
          .sort((x, y) => allLabels[x].amount - allLabels[y].amount)
      default:
        return allLabelIds.slice(0)
    }
  },
)

export const getVisibleLabelIds = createSelector(
  [getSearchTerm, getSortedLabelIds, getAllLabels],
  (searchTerm, sortedLabelIds, allLabels) => {
    if (!searchTerm || searchTerm === '') return sortedLabelIds
    // allow multiple searches seperated by comma
    const searchTerms = searchTerm.split(',')
    return sortedLabelIds.filter(id => {
      const label = allLabels[id]
      let visible = false
      // if we have multiple search terms then we want to make sure all visible
      // labels passes all the terms. So if a label does not passes all the
      // terms, it should not be visible
      let matchesAllTerm = true
      searchTerms.forEach(term => {
        if (term === '') return false
        else if (label.senderPlace.indexOf(term.toLowerCase()) === 0) {
          visible = true
        } else if (label.receiverPlace.indexOf(term.toLowerCase()) === 0) {
          visible = true
        } else if (label.amount === +term) {
          visible = true
        } else matchesAllTerm = false
      })
      return visible && matchesAllTerm
    })
  },
)

export const getSentLabelIds = createSelector(
  [getVisibleLabelIds, getAllLabels, getSelfPlace],
  (sortedLabelIds, allLabels, place) =>
    sortedLabelIds.filter(labelId => allLabels[labelId].senderPlace === place),
)

export const getReceivedLabelIds = createSelector(
  [getVisibleLabelIds, getAllLabels, getSelfPlace],
  (sortedLabelIds, allLabels, place) =>
    sortedLabelIds.filter(
      labelId => allLabels[labelId].receiverPlace === place,
    ),
)

export const getAddLabelFormState = (state: State) => state.addLabelForm

export const getExchange = createSelector(
  [getAllLabelIds, getAllLabels],
  (allLabelIds, allLabels) => {
    let totalExchange = 0
    allLabelIds.forEach(id => (totalExchange += allLabels[id].amount))
    return +totalExchange.toFixed(2)
  },
)

export const getProfit = createSelector(
  [getAllLabelIds, getAllLabels],
  (allLabelIds, allLabels) => {
    let totalProfit = 0
    allLabelIds.forEach(id => (totalProfit += allLabels[id].ownProfit))
    return +totalProfit.toFixed(2)
  },
)

export const getCreditEntries = createSelector(
  [getReceivedLabelIds, getAllLabels, getClosingBalance],
  (receivedLabelIds, allLabels, closingBalance) => {
    let creditEntries = []
    if (closingBalance < 0)
      creditEntries.push({
        place: 'closing balance',
        amount: Math.abs(closingBalance),
      })
    receivedLabelIds.forEach(id => {
      let { amount, ownProfit, senderPlace } = allLabels[id]
      let entry = {
        place: senderPlace,
        amount: +(amount + ownProfit).toFixed(2),
      }
      creditEntries.push(entry)
    })
    return creditEntries
  },
)

export const getDebitEntries = createSelector(
  [getSentLabelIds, getAllLabels, getClosingBalance],
  (sentLabelIds, allLabels, closingBalance) => {
    let debitEntries = []
    if (closingBalance > 0)
      debitEntries.push({
        place: 'closing balance',
        amount: Math.abs(closingBalance),
      })
    sentLabelIds.forEach(id => {
      let { amount, headOfficeProfit, receiverPlace } = allLabels[id]
      let entry = {
        place: receiverPlace,
        amount: +(amount + headOfficeProfit).toFixed(2),
      }
      debitEntries.push(entry)
    })
    return debitEntries
  },
)

export const getBalance = createSelector(
  [getCreditEntries, getDebitEntries],
  (creditEntries, debitEntries) => {
    let balance = 0
    creditEntries.forEach(entry => (balance -= entry.amount))
    debitEntries.forEach(entry => (balance += entry.amount))
    return +balance.toFixed(2)
  },
)

export const getCreditTotal = createSelector(
  [getCreditEntries],
  creditEntries => {
    let total = 0
    creditEntries.forEach(({ amount }) => (total += amount))
    return +total.toFixed(2)
  },
)

export const getDebitTotal = createSelector([getDebitEntries], debitEntries => {
  let total = 0
  debitEntries.forEach(({ amount }) => (total += amount))
  return +total.toFixed(2)
})
