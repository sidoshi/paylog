import { combineReducers } from 'redux'

import labelsReducer, { type State as LabelsState } from './labelsReducer'
import selfReducer, { type State as SelfState } from './selfReducer'
import addLabelFormReducer from './addLabelFormReducer'
import type { State as AddLabelFormState } from 'interfaces/addLabelForm'

export type State = {
  labels: LabelsState,
  self: SelfState,
  addLabelForm: AddLabelFormState,
}

export default combineReducers({
  labels: labelsReducer,
  self: selfReducer,
  addLabelForm: addLabelFormReducer,
})
