import rootReducer from '../'

describe('rootReducer', () => {
  it('should return an object', () => {
    expect(rootReducer(undefined, {})).toBeInstanceOf(Object)
  })
  it('should have expected keys', () => {
    let state = rootReducer(undefined, {})
    expect(Object.keys(state)).toEqual(['labels', 'self', 'addLabelForm'])
  })
})
