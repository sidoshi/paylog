import reducer from '../addLabelFormReducer'

describe('addLabelFormReducer', () => {
  let defaultState
  beforeEach(() => {
    defaultState = reducer(undefined, {})
  })
  it('should provide proper default values', () => {
    expect(defaultState).toEqual({
      senderPlace: '',
      receiverPlace: '',
      senderDisabled: true,
      receiverDisabled: false,
      amount: 0,
      charge: 0,
      ownProfit: 0,
      headOfficeProfit: 0,
      senderName: '',
      senderNumber: '',
      receiverName: '',
      receiverNumber: '',
      id: '',
    })
  })
  it('should return default state on unknown type', () => {
    expect(
      reducer(defaultState, {
        type: 'UNKNOWN',
        payload: false,
      }),
    ).toEqual(defaultState)
  })
  it('should update form state properly', () => {
    const state = { senderPlace: 'mahuva' }
    const newState = reducer(defaultState, {
      type: 'ADD_LABEL_FORM_UPDATE',
      payload: state,
    })
    let expectedState = Object.assign({}, defaultState, {
      senderPlace: 'mahuva',
    })
    expect(newState).toEqual(expectedState)
  })
  it('should reset form state properly', () => {
    const state = { senderPlace: 'mahuva' }
    const resetState = reducer(state, {
      type: 'ADD_LABEL_FORM_RESET',
    })
    expect(resetState).toEqual(defaultState)
  })
})
