import labelsReducer from '../labelsReducer'

describe('labelsReducer', () => {
  let defaultState
  beforeAll(() => {
    defaultState = labelsReducer(undefined, {})
  })
  it('should provide proper default values', () => {
    expect(labelsReducer(undefined, {})).toEqual({
      allLabels: {},
      allLabelIds: [],
      searchTerm: '',
      sortTerm: 'DATE_HIGH_TO_LOW',
    })
  })
  it('should return default state on unknown type', () => {
    expect(
      labelsReducer(defaultState, {
        type: 'UNKNOWN',
        payload: false,
      }),
    ).toEqual(defaultState)
  })
  it('should add label properly', () => {
    const label = { id: '1', sender: 'sid', receiver: 'sonu' }
    const newState = labelsReducer(defaultState, {
      type: 'LABEL_ADD',
      payload: label,
    })
    expect(newState.allLabels['1']).toEqual(label)
    expect(newState.allLabelIds[0]).toEqual('1')
    expect(newState.allLabelIds.length).toEqual(1)
  })
  it('should update label properly', () => {
    const label = { id: '1', sender: 'sid', receiver: 'sonu' }
    let newState = labelsReducer(defaultState, {
      type: 'LABEL_ADD',
      payload: label,
    })
    expect(newState.allLabels['1']).toEqual(label)
    expect(newState.allLabelIds[0]).toEqual('1')
    expect(newState.allLabelIds.length).toEqual(1)
    const updatePayload = Object.assign({}, label, { sender: 'siddharth' })
    newState = labelsReducer(newState, {
      type: 'LABEL_UPDATE',
      payload: updatePayload,
    })
    expect(newState.allLabels['1']).toMatchObject({ sender: 'siddharth' })
    expect(newState.allLabelIds[0]).toEqual('1')
    expect(newState.allLabelIds.length).toEqual(1)
  })
  it('should return given state if invalid id provided on update', () => {
    const label = { id: '1', sender: 'sid', receiver: 'sonu' }
    let newState = labelsReducer(defaultState, {
      type: 'LABEL_ADD',
      payload: label,
    })
    expect(newState.allLabels['1']).toEqual(label)
    expect(newState.allLabelIds[0]).toEqual('1')
    expect(newState.allLabelIds.length).toEqual(1)
    const updatePayload = Object.assign({}, label, {
      sender: 'siddharth',
      id: '2',
    })
    let expectedState = labelsReducer(newState, {
      type: 'LABEL_UPDATE',
      payload: updatePayload,
    })
    expect(expectedState).toEqual(newState)
  })
  it('should delete label properly', () => {
    const label = { id: '1', sender: 'sid', receiver: 'sonu' }
    let newState = labelsReducer(defaultState, {
      type: 'LABEL_ADD',
      payload: label,
    })
    expect(newState.allLabels['1']).toEqual(label)
    expect(newState.allLabelIds[0]).toEqual('1')
    expect(newState.allLabelIds.length).toEqual(1)
    newState = labelsReducer(newState, {
      type: 'LABEL_DELETE',
      payload: '1',
    })
    expect(newState.allLabels['1']).toBeUndefined()
    expect(newState.allLabelIds[0]).toBeUndefined()
    expect(newState.allLabelIds.length).toEqual(0)
  })
  it('should not delete label on invalid id', () => {
    const label = { id: '1', sender: 'sid', receiver: 'sonu' }
    let newState = labelsReducer(defaultState, {
      type: 'LABEL_ADD',
      payload: label,
    })
    expect(newState.allLabels['1']).toEqual(label)
    expect(newState.allLabelIds[0]).toEqual('1')
    expect(newState.allLabelIds.length).toEqual(1)
    newState = labelsReducer(newState, { type: 'LABEL_DELETE', payload: '2' })
    expect(newState.allLabels['1']).toEqual({
      id: '1',
      sender: 'sid',
      receiver: 'sonu',
    })
    expect(newState.allLabelIds[0]).toEqual('1')
    expect(newState.allLabelIds.length).toEqual(1)
  })
  it('should update searchTerm properly', () => {
    expect(
      labelsReducer(defaultState, {
        type: 'SEARCH_TERM_UPDATE',
        payload: 'test',
      }).searchTerm,
    ).toEqual('test')
    expect(
      labelsReducer(defaultState, {
        type: 'SEARCH_TERM_UPDATE',
        payload: '',
      }).searchTerm,
    ).toEqual('')
  })
  it('should update sortTerm properly', () => {
    expect(
      labelsReducer(defaultState, {
        type: 'SORT_TERM_UPDATE',
        payload: 'AMOUNT_LOW_TO_HIGH',
      }).sortTerm,
    ).toEqual('AMOUNT_LOW_TO_HIGH')
    expect(
      labelsReducer(defaultState, {
        type: 'DATE_HIGH_TO_LOW',
        payload: '',
      }).sortTerm,
    ).toEqual('DATE_HIGH_TO_LOW')
  })
})
