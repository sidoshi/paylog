import selfReducer from '../selfReducer'

describe('selfReducer', () => {
  let defaultState
  beforeEach(() => {
    defaultState = selfReducer(undefined, {})
  })
  it('should provide proper default values', () => {
    expect(typeof selfReducer(undefined, {}).place).toEqual('string')
  })
  it('should update self properly', () => {
    expect(
      selfReducer(defaultState, {
        type: 'SELF_UPDATE',
        payload: { place: 'rajula' },
      }),
    ).toMatchObject({ place: 'rajula' })
    expect(
      selfReducer(defaultState, {
        type: 'SELF_UPDATE',
        payload: { closingBalance: 90000 },
      }),
    ).toMatchObject({ closingBalance: 90000 })
  })
})
