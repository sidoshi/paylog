import { handleActions } from 'redux-actions'

import type { State } from 'interfaces/addLabelForm'

const defaultState: State = {
  senderPlace: '',
  receiverPlace: '',
  senderDisabled: true,
  receiverDisabled: false,
  amount: 0,
  charge: 0,
  ownProfit: 0,
  headOfficeProfit: 0,
  senderName: '',
  senderNumber: '',
  receiverName: '',
  receiverNumber: '',
  id: '',
}

export default handleActions(
  {
    ADD_LABEL_FORM_UPDATE(state, action) {
      const labelFormState = action.payload
      return Object.assign({}, state, labelFormState)
    },
    ADD_LABEL_FORM_RESET(state, action) {
      return Object.assign({}, defaultState)
    },
  },
  defaultState,
)
