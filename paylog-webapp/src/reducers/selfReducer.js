import { handleActions } from 'redux-actions'

export type State = {
  place: string,
  closingBalance: number,
  officeDate: number,
}

const defaultState: State = {
  place: '',
  closingBalance: 0,
  officeDate: Date.now(),
}

export default handleActions(
  {
    SELF_UPDATE(state, action) {
      const updatedState = action.payload
      return Object.assign({}, state, updatedState)
    },
  },
  defaultState,
)
