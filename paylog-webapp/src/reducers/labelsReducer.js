import { handleActions } from 'redux-actions'
import immutable from 'object-path-immutable'

import type { Label } from 'interfaces/label'
import type { SortTerm } from 'interfaces/visibilityFilter'

export type State = {
  allLabels: { [labelId: string]: Label },
  allLabelIds: Array<string>,
  searchTerm: string,
  sortTerm: SortTerm,
}

const defaultState: State = {
  allLabels: {},
  allLabelIds: [],
  searchTerm: '',
  sortTerm: 'DATE_HIGH_TO_LOW',
}

export default handleActions(
  {
    LABEL_ADD(state, action) {
      const id = action.payload.id
      return immutable(state)
        .set(`allLabels.${id}`, action.payload)
        .push('allLabelIds', id)
        .value()
    },
    LABEL_UPDATE(state, action) {
      const id = action.payload.id
      const oldLabel = state.allLabels[id]
      if (!oldLabel) return state
      return immutable(state)
        .set(`allLabels.${id}`, Object.assign({}, oldLabel, action.payload))
        .value()
    },
    LABEL_DELETE(state, action) {
      const id = action.payload
      const index = state.allLabelIds.indexOf(id)
      return immutable(state)
        .del(`allLabels.${id}`)
        .del(`allLabelIds.${index}`)
        .value()
    },
    LABEL_RESET(state, action) {
      return defaultState
    },
    SEARCH_TERM_UPDATE(state, action) {
      return immutable(state)
        .set('searchTerm', action.payload)
        .value()
    },
    SORT_TERM_UPDATE(state, action) {
      return immutable(state)
        .set('sortTerm', action.payload)
        .value()
    },
  },
  defaultState,
)
