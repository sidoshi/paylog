import { createStore } from 'redux'
import { throttle } from 'lodash'

import rootReducer from 'reducers'
import { loadState, saveState } from 'utils/localStorage'

const persistedState = loadState()

const store = createStore(rootReducer, persistedState)

store.subscribe(
  throttle(() => {
    const state = store.getState()
    let labels = { ...state.labels }
    /// Remove this dirty hack to not persist search and sort in localStorage
    /// You should probably move them to their own reducer
    labels.searchTerm = ''
    labels.sortTerm = 'DATE_HIGH_TO_LOW'
    /////////////////////////////////////////////////////////////////////////
    saveState({ labels: labels, self: state.self })
  }, 1000),
)

export default store
