import store from '../'

describe('Store', () => {
  it('should be an object', () => {
    expect(store).toBeInstanceOf(Object)
  })
  it('should have proper properties', () => {
    expect(store.dispatch).toBeTruthy()
    expect(store.getState).toBeTruthy()
    expect(store.subscribe).toBeTruthy()
    expect(store.replaceReducer).toBeTruthy()
  })
})
