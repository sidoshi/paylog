import {
  addLabel,
  updateLabel,
  deleteLabel,
  updateSearchTerm,
  updateSortTerm,
} from '../labelActions'

describe('addLabel', () => {
  it('should return an object', () => {
    expect(addLabel()).toBeInstanceOf(Object)
  })
  it('should have type of LABEL_ADD', () => {
    expect(addLabel().type).toEqual('LABEL_ADD')
  })
  it('should return passed arguments as payload', () => {
    expect(addLabel({ sender: 'sid' }).payload).toEqual({ sender: 'sid' })
  })
})

describe('updateLabel', () => {
  it('should return an object', () => {
    expect(updateLabel()).toBeInstanceOf(Object)
  })
  it('should have type of LABEL_ADD', () => {
    expect(updateLabel().type).toEqual('LABEL_UPDATE')
  })
  it('should return passed arguments as payload', () => {
    expect(updateLabel({ sender: 'sid' }).payload).toEqual({ sender: 'sid' })
  })
})

describe('deleteLabel', () => {
  it('should return an object', () => {
    expect(deleteLabel()).toBeInstanceOf(Object)
  })
  it('should have type of LABEL_ADD', () => {
    expect(deleteLabel().type).toEqual('LABEL_DELETE')
  })
  it('should return passed arguments as payload', () => {
    expect(deleteLabel('555').payload).toEqual('555')
  })
})

describe('updateSearchTerm', () => {
  it('should return an object', () => {
    expect(updateSearchTerm()).toBeInstanceOf(Object)
  })
  it('should have type of LABEL_ADD', () => {
    expect(updateSearchTerm().type).toEqual('SEARCH_TERM_UPDATE')
  })
  it('should return passed arguments as payload', () => {
    expect(updateSearchTerm('test').payload).toEqual('test')
  })
})

describe('updateSortTerm', () => {
  it('should return an object', () => {
    expect(updateSortTerm()).toBeInstanceOf(Object)
  })
  it('should have type of LABEL_ADD', () => {
    expect(updateSortTerm().type).toEqual('SORT_TERM_UPDATE')
  })
  it('should return passed arguments as payload', () => {
    expect(updateSortTerm('AMOUNT_HIGH_TO_LOW').payload).toEqual(
      'AMOUNT_HIGH_TO_LOW',
    )
  })
})
