import { updateSelf } from '../selfActions'

describe('updateSelf', () => {
  it('should return an object', () => {
    expect(updateSelf()).toBeInstanceOf(Object)
  })
  it('should have type of LABEL_ADD', () => {
    expect(updateSelf().type).toEqual('SELF_UPDATE')
  })
  it('should return passed arguments as payload', () => {
    expect(updateSelf({ sender: 'sid' }).payload).toEqual({ sender: 'sid' })
  })
})
