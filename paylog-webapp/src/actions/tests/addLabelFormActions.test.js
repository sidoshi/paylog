import {
  updateAddLabelForm,
  resetAddLabelForm,
  updateAddLabelFormWithLabel,
} from '../addLabelFormActions'

describe('updateAddLabelForm', () => {
  it('should return an object', () => {
    expect(updateAddLabelForm()).toBeInstanceOf(Object)
  })
  it('should have proper type', () => {
    expect(updateAddLabelForm().type).toEqual('ADD_LABEL_FORM_UPDATE')
  })
  it('should return passed arguments as payload', () => {
    expect(updateAddLabelForm({ sender: 'sid' }).payload).toEqual({
      sender: 'sid',
    })
  })
})

describe('updateAddLabelFormWithLabel', () => {
  let label
  beforeEach(() => {
    label = {
      amount: 10000,
      charge: 100,
      ownProfit: 35,
      headOfficeProfit: 65,
      sender: {
        name: 'sid',
        number: '555',
      },
      receiver: {
        name: 'sahaj',
        number: '888',
      },
      senderPlace: 'mahuva',
      receiverPlace: 'rajula',
      id: 'someid',
      selfPlace: 'mahuva',
    }
  })
  it('should return an object', () => {
    expect(updateAddLabelFormWithLabel(label)).toBeInstanceOf(Object)
  })
  it('should have proper type', () => {
    expect(updateAddLabelFormWithLabel(label).type).toEqual(
      'ADD_LABEL_FORM_UPDATE',
    )
  })
  it('should return passed arguments as payload', () => {
    expect(updateAddLabelFormWithLabel(label).payload).toEqual({
      id: label.id,
      amount: label.amount,
      charge: label.charge,
      ownProfit: label.ownProfit,
      headOfficeProfit: label.headOfficeProfit,
      senderPlace: label.senderPlace,
      receiverPlace: label.receiverPlace,
      senderName: label.sender.name,
      receiverName: label.receiver.name,
      senderNumber: label.sender.number,
      receiverNumber: label.receiver.number,
      senderDisabled: label.senderPlace === label.selfPlace,
      receiverDisabled: label.receiverPlace === label.selfPlace,
    })
  })
})

describe('resetAddLabelForm', () => {
  it('should return an object', () => {
    expect(resetAddLabelForm()).toBeInstanceOf(Object)
  })
  it('should have proper type', () => {
    expect(resetAddLabelForm().type).toEqual('ADD_LABEL_FORM_RESET')
  })
  it('should not return passed arguments as payload', () => {
    expect(resetAddLabelForm({ sender: 'sid' }).payload).toEqual(undefined)
  })
})
