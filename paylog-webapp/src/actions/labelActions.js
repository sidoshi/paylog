import { createAction } from 'redux-actions'

import type { Label } from 'interfaces/label'
import type { SortTerm } from 'interfaces/visibilityFilter'

export type AddLabelAction = {
  type: 'LABEL_ADD',
  payload: Label,
}
export type UpdateLabelAction = {
  type: 'LABEL_UPDATE',
  payload: Label,
}
export type DeleteLabelAction = {
  type: 'LABEL_DELETE',
  payload: string,
}

export type UpdateSearchTermAction = {
  type: 'SEARCH_TERM_UPDATE',
  payload: string,
}

export type UpdateSortTermAction = {
  type: 'SORT_TERM_UPDATE',
  payload: SortTerm,
}

export type Action =
  | AddLabelAction
  | UpdateLabelAction
  | DeleteLabelAction
  | UpdateSearchTermAction
  | UpdateSortTermAction

export const addLabel = (payload: Label): AddLabelAction =>
  createAction('LABEL_ADD')(payload)

export const updateLabel = (payload: Label): UpdateLabelAction =>
  createAction('LABEL_UPDATE')(payload)

export const deleteLabel = (payload: string): DeleteLabelAction =>
  createAction('LABEL_DELETE')(payload)

export const updateSearchTerm = (payload: string): UpdateSearchTermAction =>
  createAction('SEARCH_TERM_UPDATE')(payload)

export const updateSortTerm = (payload: SortTerm): UpdateSortTermAction =>
  createAction('SORT_TERM_UPDATE')(payload)
