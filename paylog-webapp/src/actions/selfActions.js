import { createAction } from 'redux-actions'

type SelfUpdatePayload = {
  place?: string,
  closingBalance?: number,
  officeDate?: number,
}

export type UpdateSelfAction = {
  type: 'SELF_UPDATE',
  payload: SelfUpdatePayload,
}

export type Action = UpdateSelfAction

export const updateSelf = (payload: SelfUpdatePayload): UpdateSelfAction =>
  createAction('SELF_UPDATE')(payload)
