import { createAction } from 'redux-actions'

import type { UpdateFormState } from 'interfaces/addLabelForm'
import type { Label } from 'interfaces/label'

export type UpdateAddLabelFormAction = {
  type: 'ADD_LABEL_FORM_UPDATE',
  payload: UpdateFormState,
}

export type ResetAddLabelFormAction = {
  type: 'ADD_LABEL_FORM_RESET',
}

export type Action = UpdateAddLabelFormAction | ResetAddLabelFormAction

export const updateAddLabelForm = (
  payload: UpdateFormState,
): UpdateAddLabelFormAction => createAction('ADD_LABEL_FORM_UPDATE')(payload)

export const updateAddLabelFormWithLabel = (
  payload: Label & { selfPlace: string },
): UpdateAddLabelFormAction => {
  const formState = {
    id: payload.id,
    amount: payload.amount,
    charge: payload.charge,
    ownProfit: payload.ownProfit,
    headOfficeProfit: payload.headOfficeProfit,
    senderPlace: payload.senderPlace,
    receiverPlace: payload.receiverPlace,
    senderName: payload.sender.name,
    receiverName: payload.receiver.name,
    senderNumber: payload.sender.number,
    receiverNumber: payload.receiver.number,
    senderDisabled: payload.senderPlace === payload.selfPlace,
    receiverDisabled: payload.receiverPlace === payload.selfPlace,
  }
  return createAction('ADD_LABEL_FORM_UPDATE')(formState)
}

export const resetAddLabelForm = (): ResetAddLabelFormAction =>
  createAction('ADD_LABEL_FORM_RESET')()
