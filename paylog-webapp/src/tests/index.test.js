import { MainApp } from '../'
import { shallow } from 'enzyme'

describe('Entry Point', () => {
  it('should run main entrypoint without crashing', () => {
    expect(shallow(MainApp).exists()).toBeTruthy()
  })
})
