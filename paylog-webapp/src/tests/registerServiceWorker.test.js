import registerServiceWorker, { unregister } from 'registerServiceWorker'

describe('registerServiceWorker', () => {
  it('registers without crashing', () => {
    expect(registerServiceWorker()).toBeTruthy()
  })
  it('unregisters without crashing', () => {
    expect(unregister()).toBeTruthy()
  })
})
