import React from 'react'
import { connect } from 'react-redux'

import {
  getCreditEntries,
  getDebitEntries,
  getCreditTotal,
  getDebitTotal,
  getBalance,
  getExchange,
  getProfit,
} from 'selectors'
import { addCommas } from 'utils/currency'
import { capitalizeFirstLetter } from 'utils/capitalizeFirstLetter'
import './AccountsPage.sass'

type EntryProps = {
  leftSide: string,
  rightSide: string,
  className?: string,
}

export const Entry = ({ leftSide, rightSide, className = '' }: EntryProps) => (
  <div className={`Entry ${className}`}>
    <span>{leftSide}</span>
    <span>{rightSide}</span>
  </div>
)

type EntryType = {
  place: string,
  amount: number,
}

type AccountsPageProps = {
  creditEntries: Array<EntryType>,
  debitEntries: Array<EntryType>,
  creditTotal: number,
  debitTotal: number,
  balance: number,
  profit: number,
  exchange: number,
}

export const getEntry = (
  { place, amount }: { place: string, amount: number },
  index: number,
) => (
  <Entry
    leftSide={capitalizeFirstLetter(place)}
    rightSide={addCommas(amount)}
    key={index}
  />
)

export const AccountsPage = ({
  creditEntries = [],
  debitEntries = [],
  creditTotal = 0,
  debitTotal = 0,
  balance = 0,
  exchange = 0,
  profit = 0,
}: AccountsPageProps) => (
  <div className="AccountsPage">
    <div className="AccountsPageInfo">
      <span>Exchange: {addCommas(exchange)}</span>
      <span>Balance: {addCommas(balance)}</span>
      <span>Profit: {addCommas(profit)}</span>
    </div>
    <div className="AccountsPageTables">
      <div className="DebitTable">
        <Entry
          className="Header"
          leftSide="Debit"
          rightSide={`Amount (${debitEntries.length})`}
        />
        <div className="EntriesList">{debitEntries.map(getEntry)}</div>
        <Entry
          className="Total"
          leftSide="Total"
          rightSide={addCommas(debitTotal)}
        />
      </div>
      <div className="CreditTable">
        <Entry
          className="Header"
          leftSide="Credit"
          rightSide={`Amount (${creditEntries.length})`}
        />
        <div className="EntriesList">{creditEntries.map(getEntry)}</div>
        <Entry
          className="Total"
          leftSide="Total"
          rightSide={addCommas(creditTotal)}
        />
      </div>
    </div>
  </div>
)

const mapStateToProps = state => ({
  creditEntries: getCreditEntries(state),
  debitEntries: getDebitEntries(state),
  debitTotal: getDebitTotal(state),
  creditTotal: getCreditTotal(state),
  balance: getBalance(state),
  exchange: getExchange(state),
  profit: getProfit(state),
})

export default connect(mapStateToProps)(AccountsPage)
