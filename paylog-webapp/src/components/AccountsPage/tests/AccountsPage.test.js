import React from 'react'
import { shallow, mount } from 'enzyme'

import { AccountsPage, Entry, getEntry } from '../'

describe('AccountsPage', () => {
  it('should render without crashing', () => {
    const wrapper = shallow(<AccountsPage />)
    expect(wrapper.exists()).toEqual(true)
  })
  it('should show credit entries properly', () => {
    const wrapper = mount(
      <AccountsPage
        creditEntries={[
          { place: 'rajula', amount: 1000 },
          { place: 'rajula', amount: 1000 },
          { place: 'rajula', amount: 1000 },
          { place: 'rajula', amount: 1000 },
        ]}
      />,
    )
    expect(wrapper.find('.CreditTable .EntriesList').exists()).toEqual(true)
    expect(wrapper.find('.CreditTable .EntriesList .Entry').length).toEqual(4)
    expect(
      wrapper
        .find('.CreditTable .EntriesList .Entry')
        .at(0)
        .text(),
    ).toEqual('Rajula1,000')
  })
  it('should show credit entries properly', () => {
    const wrapper = mount(
      <AccountsPage
        debitEntries={[
          { place: 'rajula', amount: 1000 },
          { place: 'rajula', amount: 1000 },
          { place: 'rajula', amount: 1000 },
          { place: 'rajula', amount: 1000 },
        ]}
      />,
    )
    expect(wrapper.find('.DebitTable .EntriesList').exists()).toEqual(true)
    expect(wrapper.find('.DebitTable .EntriesList .Entry').length).toEqual(4)
    expect(
      wrapper
        .find('.DebitTable .EntriesList .Entry')
        .at(0)
        .text(),
    ).toEqual('Rajula1,000')
  })
  it('should show total properly', () => {
    const wrapper = shallow(<AccountsPage />)
    expect(wrapper.find('.DebitTable .Total').exists()).toEqual(true)
    expect(wrapper.find('.CreditTable .Total').exists()).toEqual(true)
  })
})

describe('Entry', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(
      <Entry leftSide="Sid" rightSide="10,000" className="test" />,
    )
  })
  it('should render properly', () => {
    expect(wrapper.exists()).toEqual(true)
  })
  it('should have className of test and Entry', () => {
    expect(wrapper.find('.test.Entry').exists()).toEqual(true)
  })
  it('should render text as expected', () => {
    expect(
      wrapper
        .find('span')
        .at(0)
        .text(),
    ).toEqual('Sid')
    expect(
      wrapper
        .find('span')
        .at(1)
        .text(),
    ).toEqual('10,000')
  })
})

describe('getEntry', () => {
  it('should run without crashing', () => {
    expect(getEntry({ place: 'mahuva', amount: '1,000' })).toBeTruthy()
  })
  it('should return an Entry Component', () => {
    const wrapper = shallow(getEntry({ place: 'mahuva', amount: '1000' }))
    expect(wrapper.exists()).toBeTruthy()
    expect(wrapper.find('.Entry').exists()).toEqual(true)
    expect(
      wrapper
        .find('span')
        .at(0)
        .text(),
    ).toEqual('Mahuva')
    expect(
      wrapper
        .find('span')
        .at(1)
        .text(),
    ).toEqual('1,000')
  })
})
