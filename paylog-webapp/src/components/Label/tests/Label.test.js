import React from 'react'
import { shallow } from 'enzyme'

import { Label } from '../'

const getLabelProps = () => ({
  senderPlace: 'mahuva',
  receiverPlace: 'rajula',
  amount: 50000,
  charge: 50,
  sender: {
    name: 'shailesh bhai',
    number: '9898543050',
  },
  receiver: {
    name: 'manish bhai',
    number: '9898543050',
  },
  date: '10/7/2017 - 2:30 PM',
  called: false,
})

describe('Label', () => {
  it('renders properly on given required props', () => {
    const wrapper = shallow(<Label {...getLabelProps()} />)
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have a className of "Label"', () => {
    const wrapper = shallow(<Label {...getLabelProps()} />)
    expect(wrapper.hasClass('Label')).toBeTruthy()
  })
})
