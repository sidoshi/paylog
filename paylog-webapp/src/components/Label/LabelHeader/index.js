import React from 'react'
import DeleteIcon from 'react-icons/lib/md/delete'
import EditIcon from 'react-icons/lib/md/mode-edit'

import notify from 'utils/notify'
import type { Label } from 'interfaces/label'
import { capitalizeFirstLetterWithDash } from 'utils/capitalizeFirstLetter'
import './LabelHeader.sass'

type LabelHeaderProps = {
  updateLabelForm: Function,
  deleteLabel: Function,
  label: Label,
  selfPlace: string,
  index: number,
}

export const confirmAndDelete = async (id: string, deleteLabel: Function) => {
  const sure: boolean = await notify.confirm({
    title: 'Are you sure to delete that label?',
  })
  sure && deleteLabel(id)
  return true
}

export default ({
  deleteLabel,
  updateLabelForm,
  label,
  selfPlace,
  index,
}: LabelHeaderProps) => (
  <div className="LabelHeader">
    <span className="LabelPlace">
      {capitalizeFirstLetterWithDash(label.senderPlace) + ' '}
      to
      {' ' + capitalizeFirstLetterWithDash(label.receiverPlace)}
      {' - '}
      {index + 1}
    </span>
    <span className="LabelIcons">
      <span
        className="EditIcon"
        onClick={() => updateLabelForm({ ...label, selfPlace })}
      >
        <EditIcon />
      </span>
      <span
        className="DeleteIcon"
        onClick={() => confirmAndDelete(label.id, deleteLabel)}
      >
        <DeleteIcon />
      </span>
    </span>
  </div>
)
