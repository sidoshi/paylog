import React from 'react'
import { shallow, mount } from 'enzyme'

import LabelHeader, { confirmAndDelete } from '../'

const getLabelProps = () => ({
  updateLabelForm: jest.fn(),
  deleteLabel: jest.fn(),
  selfPlace: 'mahuva',
  label: { id: '1', sender: 'sid', receiver: 'sahaj' },
})

describe('LabelHeader', () => {
  it('renders properly on given required props', () => {
    const wrapper = shallow(<LabelHeader {...getLabelProps()} />)
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have a className of "LabelHeader"', () => {
    const wrapper = shallow(<LabelHeader {...getLabelProps()} />)
    expect(wrapper.hasClass('LabelHeader')).toBeTruthy()
  })
  it('should handle edit properly', () => {
    const wrapper = mount(<LabelHeader {...getLabelProps()} />)
    expect(wrapper.find('.EditIcon').exists()).toBeTruthy()
    const editButton = wrapper.find('.EditIcon')
    editButton.simulate('click')
    expect(wrapper.props().updateLabelForm).toHaveBeenCalledWith({
      id: '1',
      sender: 'sid',
      receiver: 'sahaj',
      selfPlace: 'mahuva',
    })
  })
  it('should handle delete properly', () => {
    const wrapper = mount(<LabelHeader {...getLabelProps()} />)
    expect(wrapper.find('.DeleteIcon').exists()).toBeTruthy()
  })
})

describe('confirmAndDelete', () => {
  it('should return a Promise', () => {
    expect(confirmAndDelete()).toBeInstanceOf(Promise)
  })
})
