import React from 'react'
import { storiesOf, action } from '@storybook/react'

import LabelHeader from '../'

const getLabelProps = () => ({
  label: { senderPlace: 'mahuva', receiverPlace: 'rajula' },
  selfPlace: 'mahuva',
  updateLabelForm: action('updateLabelForm'),
})

storiesOf('LabelHeader')
  .add('with normal props', () => <LabelHeader {...getLabelProps()} />)
  .add('with name with space', () => {
    let props = getLabelProps()
    props.label.receiverPlace = 'new delhi bay'
    return <LabelHeader {...props} />
  })
