import React from 'react'
import CalledIcon from 'react-icons/lib/md/phone'
import PaidIcon from 'react-icons/lib/md/payment'
import moment from 'moment'

import './LabelFooter.sass'

type LabelFooterProps = {
  date: number,
  paid?: boolean,
  called?: boolean,
}

export default ({ date, paid, called }: LabelFooterProps) => (
  <div className="LabelFooter">
    <span className="LabelDate">
      {moment(date).format('DD MMMM, YYYY - hh:mm:ss A')}
    </span>
    <span className="LabelMeta">
      <CalledIcon className={(called || paid) && 'called'} />
      {paid !== undefined && (
        <PaidIcon
          className={
            paid && 'paid' // if paid is defined only then show paidIcon
          }
        />
      )}
    </span>
  </div>
)
