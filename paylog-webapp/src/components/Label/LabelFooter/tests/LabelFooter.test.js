import React from 'react'
import { shallow } from 'enzyme'

import LabelFooter from '../'

const getLabelProps = () => ({
  date: Date.now(),
  paid: false,
  called: true,
})

describe('LabelBody', () => {
  it('renders properly on given required props', () => {
    const wrapper = shallow(<LabelFooter {...getLabelProps()} />)
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have a className of "LabelFooter"', () => {
    const wrapper = shallow(<LabelFooter {...getLabelProps()} />)
    expect(wrapper.hasClass('LabelFooter')).toBeTruthy()
  })
})
