import React from 'react'
import { storiesOf } from '@storybook/react'

import LabelFooter from '../'

const getLabelProps = () => ({
  date: Date.now(),
})

storiesOf('LabelFooter')
  .add('with normal props', () => <LabelFooter {...getLabelProps()} />)
  .add('with called=true', () => {
    let props = getLabelProps()
    props.called = true
    return <LabelFooter {...props} />
  })
  .add('with called=true, paid=false', () => {
    let props = getLabelProps()
    props.called = true
    props.paid = false
    return <LabelFooter {...props} />
  })
  .add('with paid=true', () => {
    let props = getLabelProps()
    props.paid = true
    return <LabelFooter {...props} />
  })
