import React from 'react'
import RuppeIcon from 'react-icons/lib/fa/inr'

import { addCommas } from 'utils/currency'
import { capitalizeFirstLetter } from 'utils/capitalizeFirstLetter'
import type { Client } from 'interfaces/label'

import './LabelBody.sass'

type AmountContainerProps = {
  amount: number,
  charge: number,
  ownProfit: number,
  headOfficeProfit: number,
}

type ClientContainerProps = {
  type: string,
  name: string,
  number: string,
  id: string,
}

type ClientsContainerProps = {
  sender: Client,
  receiver: Client,
}

type LabelBodyProps = AmountContainerProps & ClientsContainerProps

export const AmountContainer = ({
  amount,
  charge,
  ownProfit = 0,
  headOfficeProfit = 0,
}: AmountContainerProps) => (
  <div className="AmountContainer">
    <span className="Amount">
      Amount: {addCommas(amount)}
      <RuppeIcon />
    </span>
    <span className="Charge">
      <span className="ChargeInfo">
        Charge: {addCommas(charge)}
        <RuppeIcon />
      </span>
      <span className="ProfitInfo">
        ( {addCommas(ownProfit)} - {addCommas(headOfficeProfit)} )
      </span>
    </span>
  </div>
)

export const ClientContainer = ({
  type,
  name,
  number = '',
  id,
}: ClientContainerProps) => (
  <div className="ClientContainer" id={id}>
    <span className="ClientType">{type}</span>
    <span className="ClientName">{capitalizeFirstLetter(name) || '-'}</span>
    <span className="ClientNumber">{number || '-'}</span>
  </div>
)

export const ClientsContainer = ({
  sender,
  receiver,
}: ClientsContainerProps) => {
  if (!sender.name && !sender.number && !receiver.name && !receiver.number)
    return null
  else
    return (
      <div className="ClientsContainer">
        <ClientContainer {...sender} type="Sender" />
        <ClientContainer {...receiver} type="Receiver" />
      </div>
    )
}

export default ({
  amount,
  charge,
  ownProfit,
  headOfficeProfit,
  sender,
  receiver,
}: LabelBodyProps) => (
  <div className="LabelBody">
    <AmountContainer
      ownProfit={ownProfit}
      headOfficeProfit={headOfficeProfit}
      amount={amount}
      charge={charge}
    />
    <ClientsContainer sender={sender} receiver={receiver} />
  </div>
)
