import React from 'react'
import { shallow } from 'enzyme'

import LabelBody from '../'
import { ClientContainer, ClientsContainer, AmountContainer } from '../'

const getLabelProps = () => ({
  amount: 50000,
  charge: 50,
  sender: {
    name: 'shailesh bhai',
    number: '9898543050',
  },
  receiver: {
    name: 'manish bhai',
    number: '9898543050',
  },
})

describe('LabelBody', () => {
  it('renders properly on given required props', () => {
    const wrapper = shallow(<LabelBody {...getLabelProps()} />)
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have a className of "LabelBody"', () => {
    const wrapper = shallow(<LabelBody {...getLabelProps()} />)
    expect(wrapper.hasClass('LabelBody')).toBeTruthy()
  })
})

describe('ClientContainer', () => {
  let wrapper
  beforeEach(() => {
    const props = getLabelProps()
    wrapper = shallow(<ClientContainer type="Sender" {...props.sender} />)
  })
  it('renders properly on given required props', () => {
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have proper className', () => {
    expect(wrapper.find('.ClientContainer').exists()).toBeTruthy()
  })
  it('should have proper childs', () => {
    expect(wrapper.find('span.ClientType').exists()).toBeTruthy()
    expect(wrapper.find('span.ClientName').exists()).toBeTruthy()
    expect(wrapper.find('span.ClientNumber').exists()).toBeTruthy()
  })
})

describe('ClientsContainer', () => {
  let wrapper
  beforeEach(() => {
    const { sender, receiver } = getLabelProps()
    wrapper = shallow(<ClientsContainer sender={sender} receiver={receiver} />)
  })
  it('renders properly on given required props', () => {
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have proper className', () => {
    expect(wrapper.find('.ClientsContainer').exists()).toBeTruthy()
  })
})

describe('AmountContainer', () => {
  let wrapper
  beforeEach(() => {
    const { amount, charge } = getLabelProps()
    wrapper = shallow(<AmountContainer amount={amount} charge={charge} />)
  })
  it('renders properly on given required props', () => {
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have proper className', () => {
    expect(wrapper.find('.AmountContainer').exists()).toBeTruthy()
  })
  it('should have proper childs', () => {
    expect(wrapper.find('span.Amount').exists()).toBeTruthy()
    expect(wrapper.find('span.Charge').exists()).toBeTruthy()
  })
})
