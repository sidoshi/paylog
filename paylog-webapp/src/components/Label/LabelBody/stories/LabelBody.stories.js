import React from 'react'
import { storiesOf } from '@storybook/react'

import LabelBody from '../'

const getLabelProps = () => ({
  amount: 50000,
  charge: 50,
  sender: {
    name: 'shailesh bhai',
    number: '9898543050',
  },
  receiver: {
    name: 'manish bhai',
    number: '9898543050',
  },
})

storiesOf('LabelBody').add('with normal props', () => (
  <LabelBody {...getLabelProps()} />
))
