import React from 'react'
import { storiesOf } from '@storybook/react'
import { Provider } from 'react-redux'

import store from 'store'
import Label from '../'

const getLabelProps = () => ({
  senderPlace: 'mahuva',
  receiverPlace: 'rajula',
  amount: 50000,
  charge: 50,
  sender: {
    name: 'shailesh bhai',
    number: '9898543050',
  },
  receiver: {
    name: 'manish bhai',
    number: '9898543050',
  },
  date: Date.now(),
  called: false,
})

const LabelComponent = props => (
  <Provider store={store}>
    <Label {...props} />
  </Provider>
)

storiesOf('Label')
  .add('with normal props', () => LabelComponent(getLabelProps()))
  .add('with huge numbers', () => {
    let props = getLabelProps()
    props.amount = 4230980918230938120
    props.charge = 7234786723734
    return LabelComponent(props)
  })
  .add('with places name with space', () => {
    let props = getLabelProps()
    props.senderPlace = 'new delhi'
    return LabelComponent(props)
  })
  .add('with called=true', () => {
    let props = getLabelProps()
    props.called = true
    return LabelComponent(props)
  })
  .add('with called=true, paid=false', () => {
    let props = getLabelProps()
    props.called = true
    props.paid = false
    return LabelComponent(props)
  })
  .add('with paid=true', () => {
    let props = getLabelProps()
    props.paid = true
    return LabelComponent(props)
  })
  .add('without phone numbers', () => {
    let props = getLabelProps()
    delete props.sender.number
    delete props.receiver.number
    return LabelComponent(props)
  })
