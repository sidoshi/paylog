import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators, type Dispatch } from 'redux'

import type { Label as LabelType } from 'interfaces/label'

import { getSelfPlace } from 'selectors'
import * as addLabelFormActions from 'actions/addLabelFormActions'
import * as labelActions from 'actions/labelActions'
import LabelHeader from './LabelHeader'
import LabelBody from './LabelBody'
import LabelFooter from './LabelFooter'
import './Label.sass'

type LabelProps = LabelType & {
  deleteLabel: Function,
  updateLabelForm: Function,
  selfPlace: string,
  index: number,
}

export const Label = ({
  deleteLabel,
  updateLabelForm,
  selfPlace,
  index,
  ...label
}: LabelProps) => (
  <div className="Label" id={label.id}>
    <LabelHeader
      selfPlace={selfPlace}
      label={label}
      updateLabelForm={updateLabelForm}
      deleteLabel={deleteLabel}
      index={index}
    />
    <LabelBody
      amount={label.amount}
      charge={label.charge}
      ownProfit={label.ownProfit}
      headOfficeProfit={label.headOfficeProfit}
      sender={label.sender}
      receiver={label.receiver}
    />
    <LabelFooter date={label.date} called={label.called} paid={label.paid} />
  </div>
)

const mapStateToProps = (state: Object) => ({
  selfPlace: getSelfPlace(state),
})

type DispatchedActions = {
  updateLabelForm: Function,
  deleteLabel: Function,
}
const mapDispatchToProps = (dispatch: Dispatch<*>): DispatchedActions =>
  bindActionCreators(
    {
      updateLabelForm: addLabelFormActions.updateAddLabelFormWithLabel,
      deleteLabel: labelActions.deleteLabel,
    },
    dispatch,
  )

export default connect(mapStateToProps, mapDispatchToProps)(Label)
