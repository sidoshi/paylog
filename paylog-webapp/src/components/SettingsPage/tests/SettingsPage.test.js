import React from 'react'
import { shallow, mount } from 'enzyme'

import { SettingsPage, SettingInput } from '../'

describe('SettingsPage', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<SettingsPage />)
    expect(wrapper.exists()).toEqual(true)
  })
  it('handles submit properly', () => {
    const wrapper = mount(<SettingsPage updateSelf={jest.fn()} />)
    wrapper
      .instance()
      .handleSubmit({ branch: 'mahuva', closingBalance: '10000' })
    expect(wrapper.props().updateSelf).toHaveBeenCalledWith({
      place: 'mahuva',
      closingBalance: 10000,
    })
  })
})
describe('SettingInput', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<SettingInput />)
    expect(wrapper.exists()).toEqual(true)
  })
})
