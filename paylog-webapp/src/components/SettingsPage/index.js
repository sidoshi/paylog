import React from 'react'
import { connect } from 'react-redux'
import type { Dispatch } from 'redux'
import { LocalForm, Control } from 'react-redux-form'

import { stripCommas } from 'utils/currency'
import { capitalizeFirstLetter } from 'utils/capitalizeFirstLetter'
import * as selfActions from 'actions/selfActions'
import notify from 'utils/notify'
import { getSelfPlace, getClosingBalance } from 'selectors'
import './SettingsPage.sass'

type SettingInputProps = {
  label: string,
  model: string,
}

export const SettingInput = ({ label, model }: SettingInputProps) => (
  <div className="SettingInput">
    <label>{label}</label>
    <Control.text spellCheck="false" model={model} />
  </div>
)

export class SettingsPage extends React.Component {
  handleSubmit(values: { branch: string, closingBalance: string }) {
    let selfUpdatePayload = {
      place: ('' + values.branch).toLowerCase(),
      closingBalance: stripCommas('' + values.closingBalance),
    }
    this.props.updateSelf(selfUpdatePayload)
    notify.success({ title: 'Settings Updated' })
  }
  render = () => (
    <div className="SettingsPage">
      <div className="SettingsForm">
        <LocalForm
          onSubmit={values => this.handleSubmit(values)}
          initialState={{
            branch: capitalizeFirstLetter(this.props.selfPlace),
            closingBalance: this.props.closingAmount,
          }}
        >
          <SettingInput label="Branch" model=".branch" />
          <SettingInput label="Closing Balance" model=".closingBalance" />
          <button type="submit" className="SubmitSettings">
            Submit
          </button>
        </LocalForm>
      </div>
    </div>
  )
}

const mapStateToProps = (state: Object) => ({
  selfPlace: getSelfPlace(state),
  closingAmount: getClosingBalance(state),
})

const mapDispatchToProps = (
  dispatch: Dispatch<*>,
): { updateSelf: Function } => ({
  updateSelf: payload => dispatch(selfActions.updateSelf(payload)),
})

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage)
