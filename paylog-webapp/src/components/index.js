import React from 'react'
import { HashRouter, Route } from 'react-router-dom'

import Layout from 'components/Layout'
import './index.sass'

export default () => (
  <HashRouter>
    <Route path="/" component={Layout} />
  </HashRouter>
)
