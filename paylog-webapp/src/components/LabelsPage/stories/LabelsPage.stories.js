import React from 'react'
import { storiesOf } from '@storybook/react'
import { Provider } from 'react-redux'

import store from 'store'
import LabelsPage from '../'

storiesOf('LabelsPage').add('default', () => (
  <Provider store={store}>
    <LabelsPage />
  </Provider>
))
