import React from 'react'

import LabelsPageBar from './LabelsPageBar'
import LabelsListContainer from './LabelsListContainer'
import LabelsPageSidebar from './LabelsPageSidebar'
import './LabelsPage.sass'

export default () => (
  <div className="LabelsPage">
    <LabelsPageBar />
    <div className="LabelsPageBody">
      <LabelsPageSidebar />
      <LabelsListContainer />
    </div>
  </div>
)
