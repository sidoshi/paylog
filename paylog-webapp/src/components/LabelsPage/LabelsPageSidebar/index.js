import React from 'react'

import AddLabelForm from 'components/AddLabelForm'
import InfoPanel from '../InfoPanel'
import './LabelsPageSidebar.sass'

export default () => (
  <div className="LabelsPageSidebar">
    <AddLabelForm />
    <InfoPanel />
  </div>
)
