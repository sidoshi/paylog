import React from 'react'
import { shallow, mount } from 'enzyme'
import { Provider } from 'react-redux'

import store from 'store'
import LabelsPageSidebar from '../'

describe('LabelsPageSidebar', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<LabelsPageSidebar />)
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have AddLabelForm as a child', () => {
    const wrapper = mount(
      <Provider store={store}>
        <LabelsPageSidebar />
      </Provider>,
    )
    expect(wrapper.find('.AddLabelForm').exists()).toBeTruthy()
  })
  it('should have InfoPanel as a child', () => {
    const wrapper = mount(
      <Provider store={store}>
        <LabelsPageSidebar />
      </Provider>,
    )
    expect(wrapper.find('.InfoPanel').exists()).toBeTruthy()
  })
})
