import React from 'react'
import { storiesOf } from '@storybook/react'
import { Provider } from 'react-redux'

import store from 'store'

import LabelsPageSidebar from '../'

storiesOf('LabelsPageSidebar').add('default', () => (
  <Provider store={store}>
    <LabelsPageSidebar />
  </Provider>
))
