import React from 'react'
import { connect } from 'react-redux'

import type { Label as LabelType } from 'interfaces/label'

import { getSentLabelIds, getReceivedLabelIds, getAllLabels } from 'selectors'
import Label from 'components/Label'
import './LabelsListContainer.sass'

type AllLabels = {
  [labelId: string]: LabelType,
}

type LabelComponent = React.Element<>

type LabelsListContainerProps = {
  sentLabelIds?: Array<string>,
  receivedLabelIds?: Array<string>,
  allLabels?: AllLabels,
}

export const getLabelComponent = (
  allLabels: AllLabels,
  id: string,
  index: number,
): ?LabelComponent => {
  if (!allLabels || !id) return null
  return allLabels[id] ? (
    <Label key={id} {...allLabels[id]} index={index} />
  ) : null
}

export const getLabels = (
  labelIds: Array<string>,
  allLabels: AllLabels,
): ?Array<?LabelComponent> => {
  if (!allLabels || !labelIds) return null
  return labelIds.map(getLabelComponent.bind(null, allLabels))
}

export const LabelsListContainer = ({
  sentLabelIds = [],
  receivedLabelIds = [],
  allLabels = {},
}: LabelsListContainerProps) => (
  <div className="LabelsListContainer">
    <div className="SentLabelsList">
      <p className="ContainerInfo">Sent Labels ({sentLabelIds.length})</p>
      <div className="LabelsList">{getLabels(sentLabelIds, allLabels)}</div>
    </div>
    <div className="ReceivedLabelsList">
      <p className="ContainerInfo">
        Received Labels ({receivedLabelIds.length})
      </p>
      <div className="LabelsList">{getLabels(receivedLabelIds, allLabels)}</div>
    </div>
  </div>
)

const mapStateToProps = state => ({
  allLabels: getAllLabels(state),
  sentLabelIds: getSentLabelIds(state),
  receivedLabelIds: getReceivedLabelIds(state),
})

export default connect(mapStateToProps)(LabelsListContainer)
