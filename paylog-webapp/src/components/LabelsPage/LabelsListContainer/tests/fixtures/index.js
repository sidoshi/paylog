export const allLabels = {
  '1': {
    senderPlace: 'mahuva',
    receiverPlace: 'rajula',
    amount: 50000,
    charge: 50,
    sender: {
      name: 'shailesh bhai',
    },
    receiver: {
      name: 'manish bhai',
    },
    id: '1',
  },
  '2': {
    senderPlace: 'rajula',
    receiverPlace: 'mahuva',
    amount: 50000,
    charge: 50,
    sender: {
      name: 'shailesh bhai',
    },
    receiver: {
      name: 'manish bhai',
    },
    id: '2',
  },
}

export const sentLabelIds = ['1']
export const receivedLabelIds = ['2']
