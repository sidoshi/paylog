import React from 'react'
import { Provider } from 'react-redux'
import { shallow, mount } from 'enzyme'

import store from 'store'
import { LabelsListContainer, getLabelComponent, getLabels } from '../'
import { allLabels, sentLabelIds, receivedLabelIds } from './fixtures'

describe('LabelsListContainer', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<LabelsListContainer />)
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have a className of "LabelsListContainer"', () => {
    const wrapper = shallow(<LabelsListContainer />)
    expect(wrapper.hasClass('LabelsListContainer')).toBeTruthy()
  })
  it('should have "SentLabelsList" and ReceivedLabelsList divs', () => {
    const wrapper = shallow(<LabelsListContainer />)
    expect(wrapper.find('.SentLabelsList').exists()).toBeTruthy()
    expect(wrapper.find('.ReceivedLabelsList').exists()).toBeTruthy()
  })
  it('should have ContaierInfo inside Sent and Received Labels List', () => {
    const wrapper = shallow(<LabelsListContainer />)
    expect(
      wrapper.find('.SentLabelsList p.ContainerInfo').exists(),
    ).toBeTruthy()
    expect(
      wrapper.find('.ReceivedLabelsList p.ContainerInfo').exists(),
    ).toBeTruthy()
  })
  it('should have LabelsList inside Sent and Received Labels List', () => {
    const wrapper = shallow(<LabelsListContainer />)
    expect(wrapper.find('.SentLabelsList div.LabelsList').exists()).toBeTruthy()
    expect(
      wrapper.find('.ReceivedLabelsList div.LabelsList').exists(),
    ).toBeTruthy()
  })
  it('Should render Label Properly', () => {
    const wrapper = mount(
      <Provider store={store}>
        <LabelsListContainer
          allLabels={allLabels}
          sentLabelIds={sentLabelIds}
          receivedLabelIds={receivedLabelIds}
        />
      </Provider>,
    )
    expect(wrapper.find('.SentLabelsList .Label').exists()).toBeTruthy()
    expect(wrapper.find('.ReceivedLabelsList .Label').exists()).toBeTruthy()
    expect(wrapper.find('.SentLabelsList #1').exists()).toBeTruthy()
    expect(wrapper.find('.ReceivedLabelsList #2').exists()).toBeTruthy()
    expect(wrapper.find('.SentLabelsList span.LabelPlace').text()).toEqual(
      'Mahuva to Rajula - 1',
    )
    expect(wrapper.find('.ReceivedLabelsList span.LabelPlace').text()).toEqual(
      'Rajula to Mahuva - 1',
    )
    expect(
      wrapper.find('.SentLabelsList .LabelsList').children().length,
    ).toEqual(1)
    expect(
      wrapper.find('.ReceivedLabelsList .LabelsList').children().length,
    ).toEqual(1)
  })
})

describe('getLabelComponent', () => {
  it('returns a Label if proper id is given', () => {
    expect(getLabelComponent({ '1': { a: 'b', c: 'd' } }, '1').props.a).toEqual(
      'b',
    )
    expect(getLabelComponent({ '1': { a: 'b', c: 'd' } }, '1').props.c).toEqual(
      'd',
    )
  })
  it('should return null if label not found', () => {
    expect(getLabelComponent({ '1': 'label' }, '2')).toEqual(null)
  })
  it('should return null if allLabels in undefined', () => {
    expect(getLabelComponent(undefined, '2')).toEqual(null)
  })
  it('should return null if id is in undefined', () => {
    expect(getLabelComponent('2')).toEqual(null)
  })
})

describe('getLabels', () => {
  it('returns a Label if proper id is given', () => {
    const labelElements = getLabels([1, 2], {
      '1': { a: 'b' },
      '2': { a: 'b' },
    })
    labelElements.map(l => expect(l.props.a).toEqual('b'))
  })
  it('should return null if proper arguments not passed', () => {
    expect(getLabels()).toEqual(null)
    expect(getLabels(['1', '2'])).toEqual(null)
    expect(getLabels(null, ['1', '2'])).toEqual(null)
  })
})
