import React from 'react'
import { storiesOf } from '@storybook/react'
import { Provider } from 'react-redux'

import store from 'store'
import { LabelsListContainer } from '../'

const getLabel = ({ id, from, to }) => ({
  id: id,
  senderPlace: from,
  receiverPlace: to,
  amount: 50000,
  charge: 50,
  sender: {
    name: 'shailesh bhai',
    number: '9898543050',
  },
  receiver: {
    name: 'manish bhai',
    number: '9898543050',
  },
  date: Date.now(),
  called: false,
})

storiesOf('LabelsListContainer')
  .add('default', () => <LabelsListContainer />)
  .add('with labels', () => {
    const allLabels = {
      '1': getLabel({ id: 1, from: 'mahuva', to: 'rajula' }),
      '2': getLabel({ id: 2, from: 'mahuva', to: 'rajula' }),
      '3': getLabel({ id: 3, from: 'mahuva', to: 'rajula' }),
      '4': getLabel({ id: 4, from: 'mahuva', to: 'rajula' }),
      '5': getLabel({ id: 5, from: 'mahuva', to: 'rajula' }),
      '6': getLabel({ id: 6, from: 'mahuva', to: 'rajula' }),
      '7': getLabel({ id: 7, from: 'rajula', to: 'mahuva' }),
      '8': getLabel({ id: 8, from: 'rajula', to: 'mahuva' }),
    }
    const sentLabelIds = ['1', '2', '3', '4', '5', '6']
    const receivedLabelIds = ['7', '8']
    return (
      <Provider store={store}>
        <LabelsListContainer
          sentLabelIds={sentLabelIds}
          allLabels={allLabels}
          receivedLabelIds={receivedLabelIds}
        />
      </Provider>
    )
  })
