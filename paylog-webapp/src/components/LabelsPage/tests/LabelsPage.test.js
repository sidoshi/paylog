import React from 'react'
import { Provider } from 'react-redux'
import { shallow, mount } from 'enzyme'

import store from 'store'
import LabelsPage from '../'

describe('LabelsPage', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<LabelsPage />)
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have a className of "LabelsPage"', () => {
    const wrapper = shallow(<LabelsPage />)
    expect(wrapper.hasClass('LabelsPage')).toBeTruthy()
  })
  it('should have LabelsPageBody', () => {
    const wrapper = shallow(<LabelsPage />)
    expect(wrapper.find('.LabelsPageBody').exists()).toBeTruthy()
  })
  it('should have LabelsListContainer inside LabelsPageBody', () => {
    const wrapper = mount(
      <Provider store={store}>
        <LabelsPage />
      </Provider>,
    )
    expect(
      wrapper.find('.LabelsPageBody .LabelsListContainer').exists(),
    ).toBeTruthy()
  })
  it('should have LabelsPageSidebar inside LabelsPageBody', () => {
    const wrapper = mount(
      <Provider store={store}>
        <LabelsPage />
      </Provider>,
    )
    expect(
      wrapper.find('.LabelsPageBody .LabelsPageSidebar').exists(),
    ).toBeTruthy()
  })
})
