import React from 'react'
import { connect } from 'react-redux'
import ExchangeIcon from 'react-icons/lib/io/code'
import BalanceIcon from 'react-icons/lib/io/ios-paper-outline'
import ProfitIcon from 'react-icons/lib/io/cash'

import { getExchange, getProfit, getBalance } from 'selectors'
import { addCommas } from 'utils/currency'
import './InfoPanel.sass'

type InfoPanelProps = {
  exchange: number,
  balance: number,
  profit: number,
}

export const InfoPanel = ({
  exchange = 0,
  balance = 0,
  profit = 0,
}: InfoPanelProps) => (
  <div className="InfoPanel">
    <div>
      <span className="Field Exchange">
        <ExchangeIcon /> Exchange:{' '}
      </span>
      <span className="Value Exchange">{addCommas(exchange)}</span>
    </div>
    <div>
      <span className="Field Balance">
        <BalanceIcon /> Balance:{' '}
      </span>
      <span className="Value Balance">{addCommas(balance)}</span>
    </div>
    <div>
      <span className="Field Profit">
        <ProfitIcon /> Profit:{' '}
      </span>
      <span className="Value Profit">{addCommas(profit)}</span>
    </div>
  </div>
)

const mapStateToProps = (state: Object) => ({
  exchange: getExchange(state),
  balance: getBalance(state),
  profit: getProfit(state),
})

export default connect(mapStateToProps)(InfoPanel)
