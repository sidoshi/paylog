import React from 'react'
import { shallow } from 'enzyme'

import { InfoPanel } from '../'

describe('InfoPanel', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(
      <InfoPanel balance={1000} exchange={1000000} profit={3000} />,
    )
  })
  it('should render without crashing', () => {
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have a className of .InfoPanel', () => {
    expect(wrapper.find('.InfoPanel').exists()).toBeTruthy()
  })
  it('should have 3 Fields and Values', () => {
    expect(wrapper.find('span.Field').length).toEqual(3)
    expect(wrapper.find('span.Value').length).toEqual(3)
  })
  it('should render Exchange properly', () => {
    expect(wrapper.find('span.Value.Exchange').text()).toEqual('1,000,000')
  })
  it('should render Balance properly', () => {
    expect(wrapper.find('span.Value.Balance').text()).toEqual('1,000')
  })
  it('should render Profit properly', () => {
    expect(wrapper.find('span.Value.Profit').text()).toEqual('3,000')
  })
})
