import React from 'react'
import { storiesOf } from '@storybook/react'
import { InfoPanel } from '../'

storiesOf('InfoPanel').add('default', () => (
  <InfoPanel balance={40000} exchange={90000} profit={3000} />
))
