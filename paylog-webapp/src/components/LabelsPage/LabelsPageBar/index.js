import React from 'react'
import SearchIcon from 'react-icons/lib/md/search'
import { connect } from 'react-redux'
import { bindActionCreators, type Dispatch } from 'redux'

import { getSearchTerm, getSortTerm } from 'selectors'
import { updateSearchTerm, updateSortTerm } from 'actions/labelActions'
import './LabelsPageBar.sass'

export class LabelsPageBar extends React.Component {
  handleChange = (e: Object) => {
    const target = e.target
    let value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    switch (name) {
      case 'searchTerm':
        this.props.updateSearchTerm(value)
        break
      case 'sortTerm':
        this.props.updateSortTerm(value)
        break
      default:
        return
    }
  }
  render = () => (
    <div className="LabelsPageBar">
      <div className="SearchAndSort">
        <div className="SearchLabels">
          <label>
            <SearchIcon />{' '}
          </label>
          <input
            name="searchTerm"
            type="search"
            onChange={this.handleChange}
            value={this.props.searchTerm}
          />
        </div>
        <div className="SortLabels">
          <select
            name="sortTerm"
            onChange={this.handleChange}
            value={this.props.sortTerm}
          >
            <option value="DATE_HIGH_TO_LOW">Date (high-to-low) </option>
            <option value="DATE_LOW_TO_HIGH">Date (low-to-high) </option>
            <option value="AMOUNT_HIGH_TO_LOW">Amount (high-to-low) </option>
            <option value="AMOUNT_LOW_TO_HIGH">Amount (low-to-high) </option>
          </select>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = state => ({
  searchTerm: getSearchTerm(state),
  sortTerm: getSortTerm(state),
})

type DispatchedActions = {
  updateSearchTerm: Function,
  updateSortTerm: Function,
}

const mapDispatchToProps = (dispatch: Dispatch<*>): DispatchedActions =>
  bindActionCreators(
    {
      updateSearchTerm,
      updateSortTerm,
    },
    dispatch,
  )

export default connect(mapStateToProps, mapDispatchToProps)(LabelsPageBar)
