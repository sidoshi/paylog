import React from 'react'
import { shallow, mount } from 'enzyme'

import { LabelsPageBar } from '../'

const getLabelsPageBar = ({
  searchTerm = '',
  sortTerm = 'DATE_HIGH_TO_LOW',
  updateSearchTerm = jest.fn(),
  updateSortTerm = jest.fn(),
}) =>
  mount(
    <LabelsPageBar
      searchTerm={searchTerm}
      sortTerm={sortTerm}
      updateSearchTerm={updateSearchTerm}
      updateSortTerm={updateSortTerm}
    />,
  )

describe('LabelsPageBar', () => {
  it('renders without crashing', () => {
    const wrapper = getLabelsPageBar({})
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have a className of "LabelsPageBar"', () => {
    const wrapper = getLabelsPageBar({})
    expect(wrapper.hasClass('LabelsPageBar')).toBeTruthy()
  })
  it('should have a className of "SearchAndSort"', () => {
    const wrapper = getLabelsPageBar({})
    expect(wrapper.find('.SearchAndSort').length).toEqual(1)
  })
  describe('handleChange', () => {
    let updateSearchTerm, updateSortTerm, event, handleChange
    beforeEach(() => {
      const wrapper = getLabelsPageBar({})
      event = {
        target: {
          type: 'text',
          name: 'searchTerm',
          value: '',
        },
      }
      handleChange = wrapper.instance().handleChange
      updateSearchTerm = wrapper.props().updateSearchTerm
      updateSortTerm = wrapper.props().updateSortTerm
    })
    it('should handle update searchTerm properly', () => {
      event.target.value = 'search'
      handleChange(event)
      expect(updateSearchTerm).toHaveBeenCalledWith('search')
      event.target.value = ''
    })
    it('should handle update sortTerm properly', () => {
      event.target.name = 'sortTerm'
      event.target.value = 'sort'
      handleChange(event)
      expect(updateSortTerm).toHaveBeenCalledWith('sort')
      event.target.name = 'searchTerm'
      event.target.value = ''
    })
    it('should return on invalid name', () => {
      event.target.name = ''
      handleChange(event)
      expect(updateSortTerm).not.toHaveBeenCalled()
      expect(updateSearchTerm).not.toHaveBeenCalled()
      event.target.name = 'searchTerm'
    })
  })
})
