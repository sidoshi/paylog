import React from 'react'
import ReactAutoSuggest from 'react-autosuggest'

import './AutoSuggest.sass'

export const getSuggestionValue = (suggestion: string) => suggestion

export const renderSuggestion = (suggestion: string) => <div>{suggestion}</div>

type AutoSuggestProps = {
  label: string,
  onChange: Function,
  value: string,
  name: string,
  suggestions: Array<string>,
  onSuggestionsFetch: Function,
  onSuggestionsClear: Function,
  id: string,
  disabled: boolean,
  shouldRenderSuggestions: Function,
  setRef?: Function,
}

export default class AutoSuggest extends React.Component {
  static defaultProps: AutoSuggestProps = {
    label: '',
    onChange: () => {},
    value: '',
    name: '',
    suggestions: [''],
    onSuggestionsClear: () => {},
    onSuggestionsFetch: () => {},
    id: '',
    disabled: false,
    shouldRenderSuggestions: () => {},
    setRef: () => {},
  }
  componentDidMount() {
    this.props.setRef && this.props.setRef()
  }
  render = () => {
    const {
      label,
      onChange,
      value,
      name,
      suggestions,
      onSuggestionsFetch,
      onSuggestionsClear,
      id,
      disabled,
      shouldRenderSuggestions,
    } = this.props
    return (
      <label className="AutoSuggest">
        {label}
        <ReactAutoSuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={onSuggestionsFetch}
          onSuggestionsClearRequested={onSuggestionsClear}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          shouldRenderSuggestions={shouldRenderSuggestions}
          inputProps={{
            onChange,
            value,
            name,
            id,
            disabled,
            spellCheck: false,
          }}
        />
      </label>
    )
  }
}
