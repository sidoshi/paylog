import React from 'react'
import { shallow, mount } from 'enzyme'

import AutoSuggest, { getSuggestionValue, renderSuggestion } from '../'

describe('AutoSuggest', () => {
  let wrapper, mountWrapper
  beforeEach(() => {
    let Comp = (
      <AutoSuggest
        setRef={jest.fn()}
        label="Test"
        onChange={jest.fn()}
        value="value"
        name="name"
        suggestions={['some', 'suggestions']}
        onSuggestionsFetch={jest.fn()}
        onSuggestionsClear={jest.fn()}
        id="uniq"
        disabled={false}
        shouldRenderSuggestions={jest.fn()}
      />
    )
    wrapper = shallow(Comp)
    mountWrapper = mount(Comp)
  })
  it('should render without crashing', () => {
    expect(wrapper.exists()).toEqual(true)
  })
  it('should call setRef on mount', () => {
    mountWrapper.instance().componentDidMount()
    expect(mountWrapper.props().setRef).toHaveBeenCalled()
  })
  it('should have class AutoSuggest', () => {
    expect(wrapper.find('.AutoSuggest').exists()).toEqual(true)
  })
  it('should render label properly', () => {
    expect(wrapper.text()).toMatchSnapshot()
  })
})

describe('getSuggestionValue', () => {
  it('should return proper suggestion value', () => {
    expect(getSuggestionValue('test')).toEqual('test')
  })
})

describe('renderSuggestion', () => {
  it('should render a suggestion that does not crash', () => {
    const wrapper = shallow(renderSuggestion('test'))
    expect(wrapper.exists()).toEqual(true)
  })
  it('should render a suggestion in a div', () => {
    const wrapper = shallow(renderSuggestion('test'))
    expect(wrapper).toMatchSnapshot()
  })
})
