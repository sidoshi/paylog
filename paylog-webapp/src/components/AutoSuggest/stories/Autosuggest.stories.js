import React from 'react'
import { storiesOf, action } from '@storybook/react'
import AutoSuggest from '../'

storiesOf('AutoSuggest').add('default', () => (
  <AutoSuggest
    onChange={action('onChange')}
    value={'hello'}
    name="place"
    suggestions={['hello', 'world']}
    onSuggestionsFetch={action('onFetcn')}
    onSuggestionsClear={action('onClear')}
    setRef={action('setRef')}
    id="uniqqq"
  />
))
