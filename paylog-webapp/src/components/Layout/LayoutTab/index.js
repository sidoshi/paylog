import React from 'react'
import { NavLink } from 'react-router-dom'
import LabelsIcon from 'react-icons/lib/ti/business-card'
import AccountsIcon from 'react-icons/lib/md/account-balance-wallet'
import SettingsIcon from 'react-icons/lib/md/settings'

import './LayoutTab.sass'

type TabButtonProps = {
  link: string,
  text: string,
  Icon: any,
}

export const TabButton = ({ link, text, Icon }: TabButtonProps) => (
  <NavLink activeClassName="active" className="TabButton" to={link}>
    {Icon && <Icon size={30} />}
    {text}
  </NavLink>
)

export default () => (
  <div className="LayoutTab">
    <TabButton link="/labels" text="Labels" Icon={LabelsIcon} />
    <TabButton link="/accounts" text="Accounts" Icon={AccountsIcon} />
    <TabButton link="/settings" text="Settings" Icon={SettingsIcon} />
  </div>
)
