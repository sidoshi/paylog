import React from 'react'
import { storiesOf } from '@storybook/react'
import { BrowserRouter } from 'react-router-dom'

import LayoutTab from '../'

storiesOf('LayoutTab').add('default', () => (
  <BrowserRouter>
    <LayoutTab />
  </BrowserRouter>
))
