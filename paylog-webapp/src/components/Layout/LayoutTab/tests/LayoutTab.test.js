import React from 'react'
import { shallow } from 'enzyme'

import LayoutTab, { TabButton } from '../'

describe('LayoutTab', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<LayoutTab />)
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have a className of "LayoutTab"', () => {
    const wrapper = shallow(<LayoutTab />)
    expect(wrapper.hasClass('LayoutTab')).toBeTruthy()
  })
})

describe('TabButton', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<TabButton link="/" />)
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have a className of "TabButton"', () => {
    const wrapper = shallow(<TabButton link="/" />)
    expect(wrapper.hasClass('TabButton')).toBeTruthy()
  })
})
