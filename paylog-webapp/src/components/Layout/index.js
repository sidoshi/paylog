import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import moment from 'moment'

import notify from 'utils/notify'
import { getOfficeDate, getSelfPlace } from 'selectors'
import cleanupState from 'utils/cleanupState'
import LayoutTab from './LayoutTab'
import LabelsPage from 'components/LabelsPage'
import AccountsPage from 'components/AccountsPage'
import SettingsPage from 'components/SettingsPage'
import './Layout.sass'

export class Layout extends React.Component {
  componentDidMount() {
    const today = moment()
    const officeDate = moment(this.props.officeDate)
    if (today.isAfter(officeDate, 'day')) {
      notify.success({ title: 'Cleaning up!' })
      cleanupState()
    }
  }
  render = () => {
    return (
      <div className="Layout">
        <LayoutTab />
        <Switch>
          <Redirect exact from="/" to="/labels" />
          <Route path="/labels" component={LabelsPage} />
          <Route path="/accounts" component={AccountsPage} />
          <Route path="/settings" component={SettingsPage} />
        </Switch>
        {!this.props.selfPlace && <Redirect to="/settings" />}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  officeDate: getOfficeDate(state),
  selfPlace: getSelfPlace(state),
})

export default connect(mapStateToProps)(Layout)
