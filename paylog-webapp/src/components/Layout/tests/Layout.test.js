import React from 'react'
import { shallow } from 'enzyme'

import { Layout } from '../'

const getLayout = (officeDate = Date.now()) =>
  shallow(<Layout officeDate={officeDate} />)

describe('Layout', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<Layout />)
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have a className of "Layout"', () => {
    const wrapper = shallow(<Layout />)
    expect(wrapper.hasClass('Layout')).toBeTruthy()
  })
  describe('componentDidMount', () => {
    it('should run without crashing', () => {
      const wrapper = getLayout()
      expect(wrapper.instance().componentDidMount()).toEqual(undefined)
    })
  })
})
