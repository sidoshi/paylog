import React from 'react'
import { storiesOf } from '@storybook/react'
import { Provider } from 'react-redux'

import store from 'store'
import AddLabelForm from '../'

storiesOf('AddLabelForm').add('default', () => {
  return (
    <Provider store={store}>
      <AddLabelForm />
    </Provider>
  )
})
