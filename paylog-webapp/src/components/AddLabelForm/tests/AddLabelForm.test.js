import React from 'react'
import { shallow, mount } from 'enzyme'

import { AddLabelForm, validFormValues } from '../'

describe('AddLabelForm', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<AddLabelForm />)
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should have required inputs', () => {
    const wrapper = mount(<AddLabelForm />)
    expect(wrapper.find('.Place.FormContainer').length).toEqual(1)
    expect(wrapper.find('.Amount.FormContainer').length).toEqual(1)
    expect(wrapper.find('.Sender.FormContainer').length).toEqual(1)
    expect(wrapper.find('.Receiver.FormContainer').length).toEqual(1)
    expect(wrapper.find('.AddLabelFormButtons').length).toEqual(1)
  })
  describe('togglePlaces', () => {
    it('should toggle places properly', () => {
      const wrapper = mount(
        <AddLabelForm
          updateFormState={jest.fn()}
          formState={{
            receiverPlace: 'rajula',
            senderPlace: 'mahuva',
            senderDisabled: true,
            receiverDisabled: false,
          }}
        />,
      )
      wrapper.instance().togglePlaces()
      expect(wrapper.props().updateFormState).toHaveBeenCalledWith({
        receiverPlace: 'mahuva',
        senderPlace: 'rajula',
        senderDisabled: false,
        receiverDisabled: true,
      })
    })
  })
  describe('updateCharge', () => {
    it('should update charge properly', () => {
      const wrapper = mount(<AddLabelForm updateFormState={jest.fn()} />)
      wrapper.instance().updateCharge(1000)
      expect(wrapper.props().updateFormState).toBeCalledWith({
        charge: 50,
        headOfficeProfit: 32.5,
        ownProfit: 17.5,
      })
    })
  })
  describe('updateOwnProfit', () => {
    it('should update ownProfit properly', () => {
      const wrapper = mount(<AddLabelForm updateFormState={jest.fn()} />)
      wrapper.instance().updateOwnProfit(100)
      expect(wrapper.props().updateFormState).toBeCalledWith({
        headOfficeProfit: 65,
        ownProfit: 35,
      })
    })
  })
  describe('updateHeadOfficeProfit', () => {
    it('should update headOfficeProfit properly', () => {
      const wrapper = mount(
        <AddLabelForm
          formState={{ charge: 200 }}
          updateFormState={jest.fn()}
        />,
      )
      wrapper.instance().updateHeadOfficeProfit(10)
      expect(wrapper.props().updateFormState).toBeCalledWith({
        headOfficeProfit: 190,
      })
    })
  })
  describe('handleInputChange', () => {
    let event
    beforeEach(() => {
      event = {
        target: {
          type: 'text',
          name: 'test',
          value: 10000,
        },
      }
    })
    it('should handle input change properly', () => {
      const wrapper = mount(<AddLabelForm updateFormState={jest.fn()} />)
      wrapper.instance().handleInputChange(event)
      expect(wrapper.props().updateFormState).toHaveBeenCalledWith({
        test: 10000,
      })
    })
    it('should call update charge on amount change', () => {
      const updateFormState = jest.fn()
      const wrapper = mount(<AddLabelForm updateFormState={updateFormState} />)
      event.target.name = 'amount'
      wrapper.instance().updateCharge = jest.fn()
      wrapper.update()
      wrapper.instance().handleInputChange(event)
      expect(wrapper.instance().updateCharge).toHaveBeenCalled()
      expect(updateFormState).toHaveBeenCalledWith({ amount: 10000 })
    })
    it('should call update ownProfit on charge change', () => {
      const updateFormState = jest.fn()
      const wrapper = mount(<AddLabelForm updateFormState={updateFormState} />)
      event.target.name = 'charge'
      wrapper.instance().updateOwnProfit = jest.fn()
      wrapper.update()
      wrapper.instance().handleInputChange(event)
      expect(wrapper.instance().updateOwnProfit).toHaveBeenCalled()
    })
    it('should call updateHeadOfficeProfit on ownProfit change', () => {
      const updateFormState = jest.fn()
      const wrapper = mount(<AddLabelForm updateFormState={updateFormState} />)
      event.target.name = 'ownProfit'
      wrapper.instance().updateHeadOfficeProfit = jest.fn()
      wrapper.update()
      wrapper.instance().handleInputChange(event)
      expect(wrapper.instance().updateHeadOfficeProfit).toHaveBeenCalled()
    })
    it('should not allow invalid input', () => {
      const updateFormState = jest.fn()
      const wrapper = shallow(
        <AddLabelForm updateFormState={updateFormState} />,
      )
      event.target.name = 'amount'
      event.target.value = 'should not allow'
      wrapper.instance().handleInputChange(event)
      expect(updateFormState).not.toHaveBeenCalled()
    })
  })
  describe('handelSenderChange', () => {
    let wrapper
    let updateFormState
    beforeEach(() => {
      updateFormState = jest.fn()
      wrapper = shallow(
        <AddLabelForm
          updateFormState={updateFormState}
          formState={{
            senderPlace: 'mahuva',
          }}
        />,
      )
    })
    it('should update senderPlace properly', () => {
      wrapper.instance().handleSenderChange(null, { newValue: 'mahuv' })
      expect(updateFormState).toHaveBeenCalledWith({ senderPlace: 'mahuv' })
    })
  })
  describe('handelReceiverChange', () => {
    let wrapper
    let updateFormState
    beforeEach(() => {
      updateFormState = jest.fn()
      wrapper = shallow(
        <AddLabelForm
          updateFormState={updateFormState}
          formState={{ receiverPlace: 'mahuva' }}
        />,
      )
    })
    it('should update receiverPlace properly', () => {
      wrapper.instance().handleReceiverChange(null, { newValue: 'mahuv' })
      expect(updateFormState).toHaveBeenCalledWith({ receiverPlace: 'mahuv' })
    })
  })
  describe('onSuggestionFetch', () => {
    let wrapper
    beforeEach(() => {
      wrapper = mount(<AddLabelForm />)
    })
    it('should update suggestions properly', () => {
      wrapper.instance().onSuggestionsFetch({ value: 'a' })
      expect(wrapper.state()).toMatchObject({
        placeSuggestions: [
          'Adajan',
          'Ajmer',
          'Akola',
          'Alkapuri Baroda',
          'Amreli',
          'Anand',
          'Andheri',
          'Anjar',
          'Ankleshwar',
          'Ankola',
          'Astron Chowk',
          'Aurangabad',
        ],
      })
    })
  })
  describe('onSuggestionClear', () => {
    let wrapper
    beforeEach(() => {
      wrapper = mount(<AddLabelForm />)
    })
    it('should update suggestions properly', () => {
      wrapper.instance().onSuggestionsClear()
      expect(wrapper.state()).toMatchObject({
        placeSuggestions: [],
      })
    })
  })
  describe('shouldRenderSuggestion', () => {
    it('should not render sender suggestions on senderDisabled', () => {
      const wrapper = mount(
        <AddLabelForm formState={{ senderDisabled: true }} />,
      )
      expect(wrapper.instance().shouldRenderSenderSuggestions()).toEqual(false)
      expect(wrapper.instance().shouldRenderReceiverSuggestions()).toEqual(true)
    })
    it('should not render receiver suggestions on senderDisabled', () => {
      const wrapper = mount(
        <AddLabelForm formState={{ receiverDisabled: true }} />,
      )
      expect(wrapper.instance().shouldRenderReceiverSuggestions()).toEqual(
        false,
      )
      expect(wrapper.instance().shouldRenderSenderSuggestions()).toEqual(true)
    })
  })
  describe('clearForm', () => {
    it('should clear form properly', () => {
      const updateFormState = jest.fn()
      const wrapper = shallow(
        <AddLabelForm
          updateFormState={updateFormState}
          formState={{
            amount: 1000,
            ownProfit: 30,
            headOfficeProfit: 70,
            senderName: 'test',
            id: 'some',
          }}
        />,
      )
      wrapper.instance().clearForm()
      const newState = updateFormState.mock.calls[0][0]
      expect(newState).toMatchObject({
        amount: 0,
        senderName: '',
        id: '',
        ownProfit: 0,
        headOfficeProfit: 0,
      })
    })
  })
  describe('submitForm', () => {
    let labelState
    beforeEach(() => {
      labelState = {
        senderPlace: 'mahuva',
        receiverPlace: 'rajula',
        senderDisabled: true,
        receiverDisabled: false,
        amount: 1000,
        charge: 0,
        ownProfit: 0,
        headOfficeProfit: 0,
        senderName: 'sid',
        senderNumber: '9898543050',
        receiverName: 'sonu',
        receiverNumber: '9408180011',
      }
    })
    it('should submit form properly', () => {
      const addLabel = jest.fn()
      const wrapper = mount(
        <AddLabelForm addLabel={addLabel} formState={labelState} />,
      )
      wrapper.instance().submitForm()
      const labelArg = addLabel.mock.calls[0][0]
      expect(labelArg).toMatchObject({
        senderPlace: 'mahuva',
        receiverPlace: 'rajula',
        amount: 1000,
        charge: 0,
        ownProfit: 0,
        headOfficeProfit: 0,
        sender: {
          name: 'sid',
          number: '9898543050',
        },
        receiver: {
          name: 'sonu',
          number: '9408180011',
        },
      })
      expect(labelArg.id.length).toBeGreaterThan(6)
    })
    it('should not submit on invalid label', () => {
      const addLabel = jest.fn()
      labelState.senderPlace = ''
      const wrapper = mount(
        <AddLabelForm addLabel={addLabel} formState={labelState} />,
      )
      wrapper.instance().submitForm()
      expect(addLabel).not.toHaveBeenCalled()
    })
    it('should edit form properly', () => {
      const updateLabel = jest.fn()
      labelState.id = 'someId'
      const wrapper = mount(
        <AddLabelForm updateLabel={updateLabel} formState={labelState} />,
      )
      wrapper.instance().submitForm()
      const labelArg = updateLabel.mock.calls[0][0]
      expect(labelArg).toMatchObject({
        id: 'someId',
        senderPlace: 'mahuva',
        receiverPlace: 'rajula',
        amount: 1000,
        charge: 0,
        sender: {
          name: 'sid',
          number: '9898543050',
        },
        receiver: {
          name: 'sonu',
          number: '9408180011',
        },
      })
    })
    it('should not edit on invalid label', () => {
      const updateLabel = jest.fn()
      labelState.senderPlace = ''
      labelState.id = 'someId'
      const wrapper = mount(
        <AddLabelForm updateLabel={updateLabel} formState={labelState} />,
      )
      wrapper.instance().submitForm()
      expect(updateLabel).not.toHaveBeenCalled()
    })
  })
})

describe('validFromVaules', () => {
  it('should only allow proper amount, charge, ownProfit and headOfficeProfit values', () => {
    let names = ['amount', 'charge', 'ownProfit', 'headOfficeProfit']
    names.forEach(name => {
      expect(validFormValues[name].test(0)).toEqual(true)
      expect(validFormValues[name].test(43232712893)).toEqual(true)
      expect(validFormValues[name].test('2,2,2,2,')).toEqual(true)
      expect(validFormValues[name].test('21,34,23S')).toEqual(false)
      expect(validFormValues[name].test('s121s')).toEqual(false)
      expect(validFormValues[name].test('44444,44444,4')).toEqual(true)
      expect(validFormValues[name].test('1,00,000')).toEqual(true)
      expect(validFormValues[name].test(NaN)).toEqual(false)
    })
  })

  it('should only allow proper senderPlace and receiverPlace values', () => {
    let names = ['senderPlace', 'receiverPlace']
    names.forEach(name => {
      expect(validFormValues[name].test(0)).toEqual(true)
      expect(validFormValues[name].test('una 44')).toEqual(true)
      expect(validFormValues[name].test('Mumbai Rajula')).toEqual(true)
      expect(validFormValues[name].test('Somw-Where_else')).toEqual(true)
      expect(validFormValues[name].test('')).toEqual(true)
      expect(validFormValues[name].test('%%@#')).toEqual(false)
    })
  })

  it('should only allow proper senderName and receiverName values', () => {
    let names = ['senderName', 'receiverName']
    names.forEach(name => {
      expect(validFormValues[name].test(0)).toEqual(true)
      expect(validFormValues[name].test('Sid 1')).toEqual(true)
      expect(validFormValues[name].test('sahaj DOSHI')).toEqual(true)
      expect(validFormValues[name].test('%%@#')).toEqual(false)
      expect(validFormValues[name].test('Somw-Where_else')).toEqual(true)
      expect(validFormValues[name].test('')).toEqual(true)
    })
  })
})
