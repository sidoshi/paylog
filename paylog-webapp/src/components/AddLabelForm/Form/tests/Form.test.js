import React from 'react'
import { shallow } from 'enzyme'

import Form from '../'

describe('Form', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(<Form />)
  })
  it('should render without crashing', () => {
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should render 3 buttons', () => {
    expect(wrapper.find('button').length).toEqual(3)
  })
  it('should render submit button with proper name', () => {
    let wrapper = shallow(<Form />)
    expect(wrapper.find('button.Add').text()).toEqual('Add Label')
    wrapper = shallow(<Form id="someId" />)
    expect(wrapper.find('button.Add').text()).toEqual('Edit Label')
  })
  it('should match snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
