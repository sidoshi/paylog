import React from 'react'
import SwapIcon from 'react-icons/lib/io/arrow-swap'

import AutoSuggest from 'components/AutoSuggest'
import { addCommas } from 'utils/currency'
import { capitalizeFirstLetter as cap } from 'utils/capitalizeFirstLetter'
import Input from '../Input'
import './Form.sass'

type FormProps = {
  onChange: Function,
  from: string,
  fromDisabled: boolean,
  togglePlaces: Function,
  to: string,
  toDisabled: boolean,
  amount: number,
  charge: number,
  senderName: string,
  senderNumber: string,
  receiverName: string,
  receiverNumber: string,
  clear: Function,
  submit: Function,
  setFromInputRef: Function,
  setToInputRef: Function,
  id: string,
  ownProfit: number,
  headOfficeProfit: number,
  onSenderChange: Function,
  onReceiverChange: Function,
  placeSuggestions: Array<string>,
  onSuggestionsFetch: Function,
  onSuggestionsClear: Function,
  fromId: string,
  toId: string,
  shouldRenderSenderSuggestions: Function,
  shouldRenderReceiverSuggestions: Function,
}
export default ({
  onChange,
  from,
  fromDisabled,
  togglePlaces,
  to,
  toDisabled,
  amount,
  charge,
  senderName,
  senderNumber,
  receiverName,
  receiverNumber,
  clear,
  submit,
  setFromInputRef,
  setToInputRef,
  id,
  ownProfit,
  headOfficeProfit,
  onSenderChange,
  onReceiverChange,
  placeSuggestions,
  onSuggestionsFetch,
  onSuggestionsClear,
  fromId,
  toId,
  shouldRenderSenderSuggestions,
  shouldRenderReceiverSuggestions,
}: FormProps) => (
  <form onSubmit={submit}>
    <div className="Place FormContainer">
      <AutoSuggest
        label="From"
        onChange={onSenderChange}
        value={cap(from)}
        name="senderPlace"
        suggestions={placeSuggestions}
        onSuggestionsFetch={onSuggestionsFetch}
        onSuggestionsClear={onSuggestionsClear}
        setRef={setFromInputRef}
        id={fromId}
        disabled={fromDisabled}
        shouldRenderSuggestions={shouldRenderSenderSuggestions}
      />
      <button
        type="button"
        tabIndex="-1"
        className="ToggleButton"
        onClick={togglePlaces}
      >
        <SwapIcon size={20} />
      </button>
      <AutoSuggest
        label="To"
        onChange={onReceiverChange}
        value={cap(to)}
        name="receiverPlace"
        suggestions={placeSuggestions}
        onSuggestionsFetch={onSuggestionsFetch}
        onSuggestionsClear={onSuggestionsClear}
        setRef={setToInputRef}
        id={toId}
        disabled={toDisabled}
        shouldRenderSuggestions={shouldRenderReceiverSuggestions}
      />
    </div>

    <div className="Amount FormContainer">
      <Input
        label="Amount"
        onChange={onChange}
        name="amount"
        value={addCommas(amount)}
      />
      <Input
        label="Charge"
        onChange={onChange}
        name="charge"
        value={addCommas(charge)}
      />
    </div>

    <div className="FormContainer Profit">
      <Input
        label="Own Profit"
        onChange={onChange}
        name="ownProfit"
        value={addCommas(ownProfit)}
      />
      <Input
        label="HeadOffice Profit"
        onChange={onChange}
        name="headOfficeProfit"
        disabled={true}
        value={addCommas(headOfficeProfit)}
      />
    </div>

    <div className="Sender FormContainer">
      <Input
        label="Sender"
        onChange={onChange}
        name="senderName"
        value={cap(senderName)}
      />
      <Input
        label="Sender Number"
        onChange={onChange}
        name="senderNumber"
        value={senderNumber}
      />
    </div>
    <div className="Receiver FormContainer">
      <Input
        label="Receiver"
        onChange={onChange}
        name="receiverName"
        value={cap(receiverName)}
      />
      <Input
        label="Receiver Number"
        onChange={onChange}
        name="receiverNumber"
        value={receiverNumber}
      />
    </div>
    <div className="AddLabelFormButtons">
      <button type="button" tabIndex="-1" className="Clear" onClick={clear}>
        Clear
      </button>
      <button className="Add" type="submit">
        {id ? 'Edit Label' : 'Add Label'}
      </button>
    </div>
  </form>
)
