import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import shortid from 'shortid'
import notify from 'utils/notify'

import places from 'utils/places.json'
import { capitalizeFirstLetter as cap } from 'utils/capitalizeFirstLetter'
import { calculateProfit } from 'utils/calculateProfit'
import { stripCommas } from 'utils/currency'
import validateLabel from 'utils/validateLabel'
import calculateCharge from 'utils/calculateCharge'
import Form from './Form'
import * as labelActions from 'actions/labelActions'
import * as formActions from 'actions/addLabelFormActions'
import { getSelfPlace, getAddLabelFormState } from 'selectors'
import './AddLabelForm.sass'
import type { State as FormState } from 'interfaces/addLabelForm'

type Props = {
  selfPlace: string,
  addLabel: Function,
  updateFormState: Function,
  formState: FormState,
}

type State = {
  placeSuggestions: Array<string>,
  fromId: string,
  toId: string,
}

export const validFormValues = {
  amount: /^[\d,.]*$/,
  charge: /^[\d,.]*$/,
  ownProfit: /^[\d,.]*$/,
  headOfficeProfit: /^[\d,.]*$/,
  senderPlace: /^[\w -]*$/,
  receiverPlace: /^[\w -]*$/,
  receiverName: /^[\w -]*$/,
  senderName: /^[\w -]*$/,
}

export class AddLabelForm extends React.Component {
  static defaultProps: Props = {
    selfPlace: '',
    addLabel: () => {},
    updateFormState: () => {},
    formState: {
      id: '',
      senderPlace: '',
      receiverPlace: '',
      senderDisabled: true,
      receiverDisabled: false,
      amount: 0,
      charge: 0,
      ownProfit: 0,
      headOfficeProfit: 0,
      senderName: '',
      senderNumber: '',
      receiverName: '',
      receiverNumber: '',
    },
  }
  state: State = {
    placeSuggestions: [''],
    fromId: '',
    toId: '',
  }
  constructor() {
    super()
    this.state = {
      placeSuggestions: places,
      fromId: shortid.generate(),
      toId: shortid.generate(),
    }
  }
  toInputRef = {
    focus: () => {},
  }
  fromInputRef = {
    focus: () => {},
  }
  componentDidMount() {
    this.handleFocus()
    this.props.formState.senderDisabled
      ? this.props.updateFormState({
          senderPlace: this.props.selfPlace || '',
        })
      : this.props.updateFormState({
          receiverPlace: this.props.selfPlace || '',
        })
  }
  handleFocus = () => {
    this.props.formState.senderDisabled
      ? this.toInputRef && this.toInputRef.focus()
      : this.fromInputRef && this.fromInputRef.focus()
  }
  togglePlaces = () => {
    let { formState } = this.props
    this.props.updateFormState({
      senderPlace: formState.receiverPlace,
      receiverPlace: formState.senderPlace,
      senderDisabled: !formState.senderDisabled,
      receiverDisabled: !formState.receiverDisabled,
    })
    this.handleFocus()
  }
  updateCharge = (amount: number) => {
    const charge = calculateCharge(amount)
    const ownProfit = calculateProfit(charge)
    const headOfficeProfit = charge - ownProfit
    this.props.updateFormState({
      charge,
      ownProfit,
      headOfficeProfit,
    })
  }
  updateOwnProfit = (charge: number) => {
    const ownProfit = calculateProfit(charge)
    const headOfficeProfit = charge - ownProfit
    this.props.updateFormState({ ownProfit, headOfficeProfit })
  }
  updateHeadOfficeProfit = (ownProfit: number) => {
    const charge = this.props.formState.charge
    const headOfficeProfit = charge - ownProfit
    this.props.updateFormState({ headOfficeProfit })
  }
  handleInputChange = (event: Object) => {
    const target = event.target
    let value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    if (
      [
        'amount',
        'charge',
        'ownProfit',
        'headOfficeProfit',
        'senderPlace',
        'receiverPlace',
        'senderName',
        'receiverName',
      ].indexOf(name) >= 0
    ) {
      if (!validFormValues[name].test(value)) return
    }
    if (
      name === 'amount' ||
      name === 'charge' ||
      name === 'ownProfit' ||
      name === 'headOfficeProfit'
    )
      value = stripCommas('' + value)
    this.props.updateFormState({ [name]: value })
    if (name === 'amount') this.updateCharge(value)
    if (name === 'charge') this.updateOwnProfit(value)
    if (name === 'ownProfit') this.updateHeadOfficeProfit(value)
  }
  handleSenderChange = (event: Object, { newValue }: { newValue: string }) => {
    if (!validFormValues.senderPlace.test(newValue)) return
    this.props.updateFormState({ senderPlace: newValue })
  }
  handleReceiverChange = (
    event: Object,
    { newValue }: { newValue: string },
  ) => {
    if (!validFormValues.receiverPlace.test(newValue)) return
    this.props.updateFormState({ receiverPlace: newValue })
  }
  onSuggestionsFetch = ({ value }: { value: string }) => {
    const inputLength = value.length
    const placeSuggestions =
      inputLength === 0
        ? []
        : places
            .filter(
              place =>
                place.toLowerCase().slice(0, inputLength) ===
                value.toLowerCase(),
            )
            .map(place => cap(place))
    this.setState({
      placeSuggestions,
    })
  }
  onSuggestionsClear = () => {
    this.setState({
      placeSuggestions: [],
    })
  }
  shouldRenderSenderSuggestions = () => !this.props.formState.senderDisabled
  shouldRenderReceiverSuggestions = () => !this.props.formState.receiverDisabled
  clearForm = () => {
    let cleanState = {
      id: '',
      senderPlace: this.props.formState.senderPlace,
      receiverPlace: this.props.formState.receiverPlace,
      senderDisabled: this.props.formState.senderDisabled,
      receiverDisabled: this.props.formState.receiverDisabled,
      amount: 0,
      charge: 0,
      ownProfit: 0,
      headOfficeProfit: 0,
      senderName: '',
      senderNumber: '',
      receiverName: '',
      receiverNumber: '',
    }
    if (this.props.formState.senderDisabled) {
      cleanState.receiverPlace = ''
    } else {
      cleanState.senderPlace = ''
    }
    this.props.updateFormState(cleanState)
    this.handleFocus()
  }
  submitForm = (e: any) => {
    e && e.preventDefault()
    const { addLabel, updateLabel } = this.props
    const labelForm = this.props.formState
    const label = {
      id: labelForm.id || shortid.generate(),
      date: Date.now(),
      sender: {
        name: labelForm.senderName.toLowerCase(),
        number: labelForm.senderNumber,
      },
      receiver: {
        name: labelForm.receiverName.toLowerCase(),
        number: labelForm.receiverNumber,
      },
      amount: +labelForm.amount,
      charge: +labelForm.charge,
      ownProfit: +labelForm.ownProfit,
      headOfficeProfit: +labelForm.headOfficeProfit,
      senderPlace: labelForm.senderPlace.toLowerCase(),
      receiverPlace: labelForm.receiverPlace.toLowerCase(),
    }
    const [valid, message] = validateLabel(label)
    if (valid) {
      if (labelForm.id) {
        updateLabel(label)
        notify.success({ title: 'Label Edited' })
      } else {
        addLabel(label)
        notify.success({ title: 'Label Added' })
      }
      this.clearForm()
    } else notify.error({ title: message })
    this.handleFocus()
  }
  setFromInputRef = () => {
    this.fromInputRef = document.getElementById(this.state.fromId)
  }
  setToInputRef = () => {
    this.toInputRef = document.getElementById(this.state.toId)
  }
  render() {
    return (
      <div className="AddLabelForm">
        <Form
          id={this.props.formState.id}
          onChange={this.handleInputChange}
          togglePlaces={this.togglePlaces}
          from={this.props.formState.senderPlace}
          to={this.props.formState.receiverPlace}
          fromDisabled={this.props.formState.senderDisabled}
          toDisabled={this.props.formState.receiverDisabled}
          senderName={this.props.formState.senderName}
          senderNumber={this.props.formState.senderNumber}
          amount={this.props.formState.amount}
          charge={this.props.formState.charge}
          ownProfit={this.props.formState.ownProfit}
          headOfficeProfit={this.props.formState.headOfficeProfit}
          receiverName={this.props.formState.receiverName}
          receiverNumber={this.props.formState.receiverNumber}
          clear={this.clearForm}
          submit={this.submitForm}
          setFromInputRef={this.setFromInputRef}
          setToInputRef={this.setToInputRef}
          onSenderChange={this.handleSenderChange}
          onReceiverChange={this.handleReceiverChange}
          placeSuggestions={this.state.placeSuggestions}
          onSuggestionsFetch={this.onSuggestionsFetch}
          onSuggestionsClear={this.onSuggestionsClear}
          fromId={this.state.fromId}
          toId={this.state.toId}
          shouldRenderSenderSuggestions={this.shouldRenderSenderSuggestions}
          shouldRenderReceiverSuggestions={this.shouldRenderReceiverSuggestions}
        />
      </div>
    )
  }
}

const mapStateToProps = (state: Object) => ({
  selfPlace: getSelfPlace(state),
  formState: getAddLabelFormState(state),
})

const mapDispatchToProps = (dispatch: Function) =>
  bindActionCreators(
    {
      addLabel: labelActions.addLabel,
      updateLabel: labelActions.updateLabel,
      updateFormState: formActions.updateAddLabelForm,
    },
    dispatch,
  )

export default connect(mapStateToProps, mapDispatchToProps)(AddLabelForm)
