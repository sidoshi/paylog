import React from 'react'
import { shallow } from 'enzyme'

import Input from '../'

describe('Input', () => {
  it('should render without crashing', () => {
    const wrapper = shallow(<Input />)
    expect(wrapper.exists()).toBeTruthy()
  })
  it('should render an input', () => {
    const wrapper = shallow(<Input />)
    expect(wrapper.find('input').length).toEqual(1)
  })
  it('should render given label', () => {
    const wrapper = shallow(<Input label="test" />)
    expect(wrapper.find('label').text()).toEqual('test')
  })
})
