import React from 'react'

import './Input.sass'

type InputProps = {
  label: string,
  disabled?: boolean,
  tabIndex?: string,
  onChange?: Function,
  value: string | number,
  name: string,
  setRef?: Function,
  list?: string,
}

export default ({
  label,
  disabled,
  tabIndex,
  onChange,
  value,
  name,
  setRef,
  list,
}: InputProps) => (
  <label className="AddLabelInput">
    {label}
    <input
      spellCheck={false}
      tabIndex={tabIndex || '0'}
      type="text"
      disabled={disabled || false}
      onChange={onChange || (() => {})}
      value={value || ''}
      name={name}
      ref={setRef}
      list={list || ''}
    />
  </label>
)
