import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import store from 'store'
import App from 'components'
import registerServiceWorker from 'registerServiceWorker'

export const MainApp = (
  <Provider store={store}>
    <App />
  </Provider>
)

process.env.NODE_ENV !== 'test' &&
  ReactDOM.render(MainApp, document.getElementById('root'))
registerServiceWorker()
