export const calculateProfit = (amount: number = 0): number => amount * 35 / 100

export const calculateRemaining = (amount: number = 0): number =>
  amount - calculateProfit(amount)
