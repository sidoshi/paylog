import shortId from 'shortid'
import moment from 'moment'

import type { Label } from 'interfaces/label'

export default (label: Label): [boolean, string] => {
  // Empty label
  if (!label) return [false, 'Empty Label']

  // Invalid Sender Place
  if (!label.senderPlace) return [false, 'Empty Sender Place']
  else if (typeof label.senderPlace !== 'string')
    return [false, 'Invalid Sender Place']
  else if (label.senderPlace !== label.senderPlace.toLowerCase())
    return [false, 'Invalid Sender Place Case']

  // Invalid Receiver Place
  if (!label.receiverPlace) return [false, 'Empty Receiver Place']
  else if (typeof label.receiverPlace !== 'string')
    return [false, 'Invalid Receiver Place']
  else if (label.receiverPlace !== label.receiverPlace.toLowerCase())
    return [false, 'Invalid Receiver Place Case']

  // common sender and receiver place
  if (label.receiverPlace === label.senderPlace)
    return [false, 'Sender and Receiver place can not be same']

  // Invalid Amount
  if (
    !label.amount ||
    typeof label.amount !== 'number' ||
    !isFinite(label.amount) ||
    label.amount < 0
  )
    return [false, 'Invalid Amount']

  // Invalid Charge
  if (
    (!label.charge ||
      typeof label.charge !== 'number' ||
      !isFinite(label.charge) ||
      label.charge < 0) &&
    label.charge !== 0
  )
    return [false, 'Invalid Charge']

  // Invalid Own Profit
  if (
    (!label.ownProfit ||
      typeof label.ownProfit !== 'number' ||
      !isFinite(label.ownProfit) ||
      label.ownProfit > label.charge ||
      label.ownProfit < 0) &&
    label.ownProfit !== 0
  )
    return [false, 'Invalid Own Profit']

  // Invalid Head Office Profit
  if (
    (!label.headOfficeProfit ||
      typeof label.headOfficeProfit !== 'number' ||
      !isFinite(label.headOfficeProfit) ||
      label.headOfficeProfit > label.charge ||
      label.headOfficeProfit < 0) &&
    label.headOfficeProfit !== 0
  )
    return [false, 'Invalid Head Office Profit']

  // Total of HeadOffice and OwnProfit
  if (label.ownProfit + label.headOfficeProfit !== label.charge)
    return [
      false,
      'Total of Own Profit and Head Office Profit should be equal to Charge',
    ]

  // Invalid Sender Name
  if (typeof label.sender.name !== 'string')
    return [false, 'Invalid Sender Name']
  else if (label.sender.name !== label.sender.name.toLowerCase())
    return [false, 'Invalid Sender Name Case']

  // Invalid Receiver Name
  if (typeof label.receiver.name !== 'string')
    return [false, 'Invalid Receiver Name']
  else if (label.receiver.name !== label.receiver.name.toLowerCase())
    return [false, 'Invalid Receiver Name Case']

  // Invalid Id
  if (!shortId.isValid(label.id)) return [false, 'Invalid Id']

  // Invalid date
  if (!moment(label.date).isValid()) return [false, 'Invalid Date']

  // Valid! Everything looks fine!! 👍
  return [true, 'Label Added']
}
