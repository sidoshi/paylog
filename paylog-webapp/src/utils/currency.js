export const addCommas = (n: number = 0): string => {
  return (+n).toLocaleString('en-IN')
}

export const stripCommas = (s: string = '') => +s.replace(/,/g, '')
