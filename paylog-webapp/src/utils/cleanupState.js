import store from 'store'
import { getBalance } from 'selectors'

export default () => {
  const state = store.getState()
  const balance = getBalance(state)
  store.dispatch({ type: 'LABEL_RESET' })
  store.dispatch({
    type: 'SELF_UPDATE',
    payload: {
      closingBalance: balance,
      officeDate: Date.now(),
    },
  })
}
