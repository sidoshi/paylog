import notify from 'izitoast'
import 'izitoast/dist/css/iziToast.min.css'
import './notify.sass'

type DefaultOptions = {
  progressBar: boolean,
  timeout: number,
}
const defaultOptions: DefaultOptions = {
  progressBar: false,
  timeout: 2500,
}

type SuccessOptions = {
  title?: string,
  message?: string,
  timeout?: number | false,
  progressBar?: boolean,
}

export const success = (options: SuccessOptions) => {
  notify.success({ ...defaultOptions, title: 'Success', ...options })
  return true
}

type ErrorOptions = {
  title?: string,
  message?: string,
  timeout?: number | false,
  progressBar?: boolean,
}

export const error = (options: ErrorOptions) => {
  notify.error({ ...defaultOptions, title: 'Error', ...options })
  return true
}

type ConfirmOptions = {
  title: string,
}
export const confirm = (options: ConfirmOptions) =>
  new Promise((res, rej) => {
    notify.show({
      color: 'dark',
      timeout: false,
      title: 'Sure, You wanna do that?',
      position: 'bottomCenter',
      progressBar: false,
      close: false,
      buttons: [
        [
          '<button>Yes</button>',
          (instance, toast) => {
            res(true)
            success({ title: 'Label Deleted' })
            instance.hide({ transitionOut: 'fadeOutDown' }, toast, 'close')
          },
        ],
        [
          '<button>No</button>',
          (instance, toast) => {
            success({ title: 'Canceled!' })
            res(false)
            instance.hide({ transitionOut: 'fadeOutDown' }, toast, 'close')
          },
        ],
      ],
      ...options,
    })
  })

export default {
  success,
  error,
  confirm,
}
