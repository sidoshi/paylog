import type { State as LabelsState } from 'reducers/labelsReducer'
import type { State as SelfState } from 'reducers/selfReducer'

export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state')
    if (!serializedState) return undefined
    return JSON.parse(serializedState)
  } catch (err) {
    return undefined
  }
}

type PersistedState = {
  labels?: LabelsState,
  self: SelfState,
}

export const saveState = (state: PersistedState) => {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem('state', serializedState)
    return true
  } catch (err) {
    return false
  }
}
