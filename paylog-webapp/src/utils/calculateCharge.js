export default (amount: number = 0): number => {
  let charge = Math.ceil(amount * 1 / 1000)
  if (charge < 50) return 50
  let remainder = charge % 10
  if (remainder !== 0) charge += 10 - remainder
  return charge
}
