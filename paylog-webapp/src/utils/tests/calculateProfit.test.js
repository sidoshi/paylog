import { calculateProfit, calculateRemaining } from '../calculateProfit'

describe('calculateProfit', () => {
  it('should return proper profit on charge', () => {
    expect(calculateProfit(100)).toEqual(35)
    expect(calculateProfit(5000)).toEqual(1750)
    expect(calculateProfit(3333344444)).toEqual(1166670555.4)
  })
})

describe('calculateRemaining', () => {
  it('should return proper remaining on charge', () => {
    expect(calculateRemaining(100)).toEqual(65)
    expect(calculateRemaining(5000)).toEqual(3250)
    expect(calculateRemaining(3333344444)).toEqual(2166673888.6)
  })
})
