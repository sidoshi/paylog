import validateLabel from '../validateLabel'
import shortId from 'shortid'
import moment from 'moment'

describe('validateLabel', () => {
  let label
  beforeEach(() => {
    label = {
      id: shortId.generate(),
      senderPlace: 'mahuva',
      receiverPlace: 'rajula',
      amount: 100000,
      charge: 100,
      ownProfit: 35,
      headOfficeProfit: 65,
      receiver: {
        name: 'sid',
      },
      sender: {
        name: 'sid',
      },
    }
  })
  it('should return false if no Label is passed', () => {
    expect(validateLabel()).toEqual([false, 'Empty Label'])
  })
  it('should return false is senderPlace is not defined or empty', () => {
    label.senderPlace = ''
    expect(validateLabel(label)).toEqual([false, 'Empty Sender Place'])
    delete label.senderPlace
    expect(validateLabel(label)).toEqual([false, 'Empty Sender Place'])
    label.senderPlace = 555
    expect(validateLabel(label)).toEqual([false, 'Invalid Sender Place'])
    label.senderPlace = 'mahuva'
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
  })
  it('should return false is receiverPlace is not defined or empty', () => {
    label.receiverPlace = ''
    expect(validateLabel(label)).toEqual([false, 'Empty Receiver Place'])
    delete label.receiverPlace
    expect(validateLabel(label)).toEqual([false, 'Empty Receiver Place'])
    label.receiverPlace = 555
    expect(validateLabel(label)).toEqual([false, 'Invalid Receiver Place'])
    label.receiverPlace = 'rajula'
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
  })
  it('should return false is amount is not defined or empty', () => {
    label.amount = 0
    expect(validateLabel(label)).toEqual([false, 'Invalid Amount'])
    delete label.amount
    expect(validateLabel(label)).toEqual([false, 'Invalid Amount'])
    label.amount = 'string not expected'
    expect(validateLabel(label)).toEqual([false, 'Invalid Amount'])
    label.amount = Infinity
    expect(validateLabel(label)).toEqual([false, 'Invalid Amount'])
    label.amount = -1000
    expect(validateLabel(label)).toEqual([false, 'Invalid Amount'])
    label.amount = NaN
    expect(validateLabel(label)).toEqual([false, 'Invalid Amount'])
    label.amount = 10000
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
  })
  it('should return false is ownProfit is not defined or empty', () => {
    delete label.ownProfit
    expect(validateLabel(label)).toEqual([false, 'Invalid Own Profit'])
    label.ownProfit = 'string not expected'
    expect(validateLabel(label)).toEqual([false, 'Invalid Own Profit'])
    label.ownProfit = Infinity
    expect(validateLabel(label)).toEqual([false, 'Invalid Own Profit'])
    label.ownProfit = NaN
    expect(validateLabel(label)).toEqual([false, 'Invalid Own Profit'])
    label.ownProfit = -100
    expect(validateLabel(label)).toEqual([false, 'Invalid Own Profit'])
    label.ownProfit = 101
    expect(validateLabel(label)).toEqual([false, 'Invalid Own Profit'])
    label.ownProfit = 0
    label.headOfficeProfit = 100
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
    label.ownProfit = 100
    label.headOfficeProfit = 0
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
    label.ownProfit = 10
    label.headOfficeProfit = 90
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
  })
  it('should return false is headOfficeProfit is not defined or empty', () => {
    delete label.headOfficeProfit
    expect(validateLabel(label)).toEqual([false, 'Invalid Head Office Profit'])
    label.headOfficeProfit = 'string not expected'
    expect(validateLabel(label)).toEqual([false, 'Invalid Head Office Profit'])
    label.headOfficeProfit = Infinity
    expect(validateLabel(label)).toEqual([false, 'Invalid Head Office Profit'])
    label.headOfficeProfit = NaN
    expect(validateLabel(label)).toEqual([false, 'Invalid Head Office Profit'])
    label.headOfficeProfit = -100
    expect(validateLabel(label)).toEqual([false, 'Invalid Head Office Profit'])
    label.headOfficeProfit = 101
    expect(validateLabel(label)).toEqual([false, 'Invalid Head Office Profit'])
    label.headOfficeProfit = 90
    label.ownProfit = 11
    expect(validateLabel(label)).toEqual([
      false,
      'Total of Own Profit and Head Office Profit should be equal to Charge',
    ])
    label.headOfficeProfit = 0
    label.ownProfit = 100
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
    label.headOfficeProfit = 100
    label.ownProfit = 0
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
    label.headOfficeProfit = 10
    label.ownProfit = 90
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
  })
  it('should return false is charge is not defined or empty', () => {
    delete label.charge
    expect(validateLabel(label)).toEqual([false, 'Invalid Charge'])
    label.charge = 'string not expected'
    expect(validateLabel(label)).toEqual([false, 'Invalid Charge'])
    label.charge = Infinity
    expect(validateLabel(label)).toEqual([false, 'Invalid Charge'])
    label.charge = NaN
    expect(validateLabel(label)).toEqual([false, 'Invalid Charge'])
    label.charge = -100
    expect(validateLabel(label)).toEqual([false, 'Invalid Charge'])
    label.charge = 0
    label.ownProfit = 0
    label.headOfficeProfit = 0
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
    label.charge = 10
    label.ownProfit = 5
    label.headOfficeProfit = 5
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
  })
  it('should return false is receiver.name is not defined or empty', () => {
    label.receiver.name = ''
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
    delete label.receiver.name
    expect(validateLabel(label)).toEqual([false, 'Invalid Receiver Name'])
    label.receiver.name = 555
    expect(validateLabel(label)).toEqual([false, 'Invalid Receiver Name'])
    label.receiver.name = 'rajula'
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
  })
  it('should return false is sender.name is not defined or empty', () => {
    label.sender.name = ''
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
    delete label.sender.name
    expect(validateLabel(label)).toEqual([false, 'Invalid Sender Name'])
    label.sender.name = 555
    expect(validateLabel(label)).toEqual([false, 'Invalid Sender Name'])
    label.sender.name = 'rajula'
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
  })
  it('should return false is id is defined or empty', () => {
    label.id = ''
    expect(validateLabel(label)).toEqual([false, 'Invalid Id'])
    delete label.id
    expect(validateLabel(label)).toEqual([false, 'Invalid Id'])
    label.id = 555
    expect(validateLabel(label)).toEqual([false, 'Invalid Id'])
    label.id = 'dsada'
    expect(validateLabel(label)).toEqual([false, 'Invalid Id'])
    label.id = shortId.generate()
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
  })
  it('should receive all strings in lower case', () => {
    label.senderPlace = 'Mahuva'
    expect(validateLabel(label)).toEqual([false, 'Invalid Sender Place Case'])
    label.senderPlace = 'mahuva'
    label.receiverPlace = 'Rajula'
    expect(validateLabel(label)).toEqual([false, 'Invalid Receiver Place Case'])
    label.receiverPlace = 'rajula'
    label.sender.name = 'Sid'
    expect(validateLabel(label)).toEqual([false, 'Invalid Sender Name Case'])
    label.sender.name = 'sid'
    label.receiver.name = 'Sonu'
    expect(validateLabel(label)).toEqual([false, 'Invalid Receiver Name Case'])
    label.receiver.name = 'sonu'
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
  })
  it('should not have common sender and receiver place', () => {
    label.senderPlace = 'mahuva'
    label.receiverPlace = 'mahuva'
    expect(validateLabel(label)).toEqual([
      false,
      'Sender and Receiver place can not be same',
    ])
  })
  it('should have a valid date value', () => {
    label.date = moment.invalid()
    expect(validateLabel(label)).toEqual([false, 'Invalid Date'])
    label.date = Date.now()
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
  })
  it('should return true if proper label is passed', () => {
    expect(validateLabel(label)).toEqual([true, 'Label Added'])
  })
})
