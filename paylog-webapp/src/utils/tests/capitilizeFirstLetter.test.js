import {
  capitalizeFirstLetter,
  capitalizeFirstLetterWithDash,
} from '../capitalizeFirstLetter'

describe('capitalizeFirstLetter', () => {
  it('should capitalize first letter', () => {
    expect(capitalizeFirstLetter('hello')).toEqual('Hello')
  })
  it('should capitalize first letter of multiple stirngs', () => {
    expect(capitalizeFirstLetter('hello world')).toEqual('Hello World')
  })
})

describe('capitalizeFirstLetterWithDash', () => {
  it('should capitalize first letter', () => {
    expect(capitalizeFirstLetterWithDash('hello')).toEqual('Hello')
  })
  it('should capitalize first letter of multiple stirngs adding dash', () => {
    expect(capitalizeFirstLetterWithDash('hello world')).toEqual('Hello-World')
  })
})
