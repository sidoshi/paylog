import { addCommas, stripCommas } from '../currency'

describe('addCommas', () => {
  it('should add commas if number is more than 3 digits', () => {
    expect(addCommas(1000)).toEqual('1,000')
  })
  it('should add commas in indian `Lakhs` style ', () => {
    expect(addCommas(200000)).toEqual('200,000')
  })
  it('should return same string if less then 3 digits are passed', () => {
    expect(addCommas(100)).toEqual('100')
  })
  it('should return empty string if no value passed', () => {
    expect(addCommas()).toEqual('0')
  })
  it('should handel decimals properly', () => {
    expect(addCommas(20.52)).toEqual('20.52')
    expect(addCommas(20000.99)).toEqual('20,000.99')
  })
  it('should handel negative numbers properly', () => {
    expect(addCommas(-235)).toEqual('-235')
    expect(addCommas(-1000)).toEqual('-1,000')
  })
})

describe('stripCommas', () => {
  it('should strip commas and return a number', () => {
    expect(stripCommas('1,000')).toEqual(1000)
  })
})
