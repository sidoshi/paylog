import notify from '../notify'
import { success, error, confirm } from '../notify'

describe('notify', () => {
  it('should have all required properites', () => {
    expect(typeof notify.success).toEqual('function')
    expect(typeof notify.error).toEqual('function')
    expect(typeof notify.confirm).toEqual('function')
  })
})

describe('success', () => {
  it('should have be a function', () => {
    expect(typeof success).toEqual('function')
  })
  it('should run without crashing', () => {
    expect(success()).toEqual(true)
  })
})

describe('error', () => {
  it('should have be a function', () => {
    expect(typeof error).toEqual('function')
  })
  it('should run without crashing', () => {
    expect(error()).toEqual(true)
  })
})

describe('confirm', () => {
  it('should have be a function', () => {
    expect(typeof confirm).toEqual('function')
  })
  it('should return a promise', () => {
    expect(confirm()).toBeInstanceOf(Promise)
  })
})
