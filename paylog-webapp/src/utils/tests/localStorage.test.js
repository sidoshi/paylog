import { loadState, saveState, clearState } from '../localStorage'

describe('localStorage', () => {
  let localStorage
  beforeEach(() => {
    localStorage = {
      getItem: jest.fn(),
      setItem: jest.fn(),
    }
    global.localStorage = localStorage
  })
  afterEach(() => {
    global.localStorage = window.localStorage
  })
  describe('loadState', () => {
    it('should call localStorage', () => {
      loadState()
      expect(localStorage.getItem).toHaveBeenCalledWith('state')
    })
    it('should return undefined on errors', () => {
      global.localStorage = ''
      expect(loadState()).toEqual(undefined)
    })
  })
  describe('saveState', () => {
    it('should call localStorage', () => {
      const state = { hello: 'world' }
      saveState(state)
      expect(localStorage.setItem).toHaveBeenCalledWith(
        'state',
        JSON.stringify(state),
      )
    })
    it('should return false on errors', () => {
      global.localStorage = ''
      expect(saveState()).toEqual(false)
    })
  })
})
