import calculateCharge from '../calculateCharge'

describe('calculateCharge', () => {
  it('should return proper charge on amount', () => {
    expect(calculateCharge(100)).toEqual(50)
    expect(calculateCharge(5000)).toEqual(50)
    expect(calculateCharge(41000)).toEqual(50)
    expect(calculateCharge(50001)).toEqual(60)
    expect(calculateCharge(60001)).toEqual(70)
    expect(calculateCharge(100000)).toEqual(100)
    expect(calculateCharge(100001)).toEqual(110)
    expect(calculateCharge(1000000)).toEqual(1000)
    expect(calculateCharge(1010000)).toEqual(1010)
    expect(calculateCharge(1010001)).toEqual(1020)
    expect(calculateCharge(500000)).toEqual(500)
    expect(calculateCharge(500007)).toEqual(510)
    expect(calculateCharge(600017)).toEqual(610)
  })
})
