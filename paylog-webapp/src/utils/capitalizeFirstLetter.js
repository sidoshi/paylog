const _capitalizeFirstLetter = (s: string = ''): string =>
  s.charAt(0).toUpperCase() + s.slice(1)

export const capitalizeFirstLetter = (s: string = ''): string =>
  s
    .split(' ')
    .map(s => _capitalizeFirstLetter(s))
    .join(' ')

export const capitalizeFirstLetterWithDash = (s: string = ''): string =>
  s
    .split(' ')
    .map(s => _capitalizeFirstLetter(s))
    .join('-')
