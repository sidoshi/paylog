export type Client = {
  id?: string,
  name: string,
  number?: string,
}

export type Label = {
  id: string,
  senderPlace: string,
  receiverPlace: string,
  sender: Client,
  receiver: Client,
  amount: number,
  charge: number,
  ownProfit: number,
  headOfficeProfit: number,
  date: number,
  called?: boolean,
  paid?: boolean,
}
