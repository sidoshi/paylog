export type SortTerm =
  | 'AMOUNT_LOW_TO_HIGH'
  | 'AMOUNT_HIGH_TO_LOW'
  | 'DATE_LOW_TO_HIGH'
  | 'DATE_HIGH_TO_LOW'
