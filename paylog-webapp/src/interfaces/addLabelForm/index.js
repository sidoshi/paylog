export type UpdateFormState = {
  +senderPlace?: string,
  +receiverPlace?: string,
  +senderDisabled?: boolean,
  +receiverDisabled?: boolean,
  +amount?: number,
  +charge?: number,
  +ownProfit?: number,
  +headOfficeProfit?: number,
  +senderName?: string,
  +senderNumber?: string,
  +receiverName?: string,
  +receiverNumber?: string,
  +id?: string,
}

export type State = {
  +senderPlace: string,
  +receiverPlace: string,
  +senderDisabled: boolean,
  +receiverDisabled: boolean,
  +amount: number,
  +charge: number,
  +ownProfit: number,
  +headOfficeProfit: number,
  +senderName: string,
  +senderNumber: string,
  +receiverName: string,
  +receiverNumber: string,
  +id: string,
}
