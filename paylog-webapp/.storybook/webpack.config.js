const genDefaultConfig = require('@storybook/react/dist/server/config/defaults/webpack.config.js')
const path = require('path')

module.exports = (baseConfig, env) => {
  const config = genDefaultConfig(baseConfig, env)
  config.resolve.modules = ['src', 'node_modules']
  config.module.rules.push({
    test: /\.(scss|sass|css)/,
    loaders: [
      'style-loader',
      'css-loader',
      {
        loader: require.resolve('sass-loader'),
        options: {
          includePaths: [path.resolve(__dirname, '../src/sass')],
        },
      },
    ],
    include: path.resolve(__dirname, '../src'),
  })
  return config
}
