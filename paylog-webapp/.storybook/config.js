import { configure, addDecorator } from '@storybook/react'
import React from 'react'
import 'babel-polyfill'

const req = require.context('../src/', true, /\.stories\.js$/)

const styles = {
  width: '100%',
  height: '100%',
  position: 'absolute',
  top: 0,
  left: 0,
}

addDecorator(story =>
  <div style={styles}>
    {story()}
  </div>,
)

function loadStories() {
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
